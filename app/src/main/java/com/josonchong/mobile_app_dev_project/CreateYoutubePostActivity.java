package com.josonchong.mobile_app_dev_project;

import android.annotation.TargetApi;
import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

import es.dmoral.toasty.Toasty;

//Reference: https://www.youtube.com/watch?v=W4hTJybfU7s
//Reference: https://developers.google.com/youtube/android/player/downloads/
public class CreateYoutubePostActivity extends YouTubeBaseActivity {
    EditText titleET;
    EditText contentET;
    EditText youtubeLinkET;
    TextView openTV;
    ImageView ytIV;
    ImageView crossIV;
    private static final String API_KEY = "AIzaSyA9vNt6uq4ZSTBBzA6yTRBeeOJr9-ANSbA";
    YouTubePlayerView youTubePlayerView;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    FirebaseFirestore db = FirebaseFirestore.getInstance();;
    private final String TAG = "CreateYTPostActivity";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_youtube_post);
        youTubePlayerView = findViewById(R.id.youtubePlayerView);
        titleET = findViewById(R.id.titleET);
        contentET = findViewById(R.id.contentET);
        youtubeLinkET = findViewById(R.id.youtubeLinkET);
        openTV = findViewById(R.id.openTV);
        ytIV = findViewById(R.id.googleDocIV);
        crossIV = findViewById(R.id.crossIV);
        dialog = new ProgressDialog(this, android.app.AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        System.out.println("type: " + type);
        System.out.println("action: " + action);


        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.equals("text/plain")) {
                Bundle bundle = getIntent().getExtras();
                final String text = bundle.getString(Intent.EXTRA_TEXT);
                if (text.contains("youtu") || text.contains("watch?v=")) {
                    youtubeLinkET.setText(text);
                    playVideo();
                }
            }
        }

        ytIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(CreateYoutubePostActivity.this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Do you really want to leave this page?");
                alertBuilder.setMessage("Your changes will be discarded.");
                alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://www.youtube.com"));
                        startActivity(intent);
                        finish();
                    }
                });
                alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    } });
                AlertDialog alert = alertBuilder.create();
                alert.show();
            }
        });

        youtubeLinkET.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                decideVisibility();
            }
        });

        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leave();
            }
        });
        decideVisibility();
    }

    //if youtubeLinkET is empty, show the button directs users to YouTube application
    private void decideVisibility(){
        if(youtubeLinkET.getText().toString().equals("")){
            openTV.setVisibility(View.VISIBLE);
            ytIV.setVisibility(View.VISIBLE);
        }else{
            openTV.setVisibility(View.GONE);
            ytIV.setVisibility(View.GONE);
        }
    }

    //play video with url
    void playVideo(String url){
        youTubePlayerView.setVisibility(View.VISIBLE);
        final String videoUrl = url;
        onInitializedListener = new YouTubePlayer.OnInitializedListener(){
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b){
                try{
                    youTubePlayer.cueVideo(videoUrl);
                }catch (Exception ex){
                    Toasty.error(getApplicationContext(), "Sorry, unexpected error(s) occurred. Please try again later or contact customer support", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult){
            }
        };
        youTubePlayerView.initialize(API_KEY, onInitializedListener);
    }

    public void preview(View view){
        playVideo();
    }

    //play video with the link at youtubeLinkET
    private void playVideo(){
        String text = youtubeLinkET.getText().toString();
        if(text.contains("youtu.be")){
            String[] textSplit= text.split("/");
            playVideo(textSplit[textSplit.length-1]);
        }else if(text.contains("watch?v=")){
            String[] textSplit= text.split("=");
            System.out.println("textSplit[textSplit.length-1]: " + textSplit[textSplit.length-1]);
            playVideo(textSplit[textSplit.length-1]);
        }
    }

    //upload post to Firestore
    public void submit(View view){
        final String title = titleET.getText().toString();
        final String content = contentET.getText().toString();
        final String youtubeLink = youtubeLinkET.getText().toString();
        System.out.println("getCurrentDateTime() " + Helper.getCurrentDateTime());

        if (!title.equals("")&&!content.equals("")&&!youtubeLink.equals("")){
            if(youtubeLink.contains("youtu")) {
                YoutubePost newYoutubePost = new YoutubePost(CurrentSession.currentUser, Helper.getCurrentDateTime(), Helper.getCurrentDateTime(), title, content, youtubeLink);
                ObjectMapper oMapper = new ObjectMapper();
                Map<String, Object> youtubePostMap = oMapper.convertValue(newYoutubePost, Map.class);
                //.System.out.println(youtubePostMap);
                dialog.show();
                db.collection("posts")
                        .add(youtubePostMap)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                Helper.addDataToFirestoreAsyn("posts", documentReference.getId(), "documentID", documentReference.getId());
                                Toasty.success(CreateYoutubePostActivity.this, "Your post has been created.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toasty.error(CreateYoutubePostActivity.this, "Unexpected error occurred.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        });
            }else{
                Toasty.error(CreateYoutubePostActivity.this, "Incorrect link format.", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toasty.error(CreateYoutubePostActivity.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //dismiss dialog if it is showing
    private void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    //override onBackPressed
    @Override
    public void onBackPressed(){
        leave();
    }

    //Confirm if user wants to leave this page
    private void leave(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(CreateYoutubePostActivity.this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Do you really want to leave this page?");
        alertBuilder.setMessage("Your changes will be discarded.");
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            } });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    //finish activity if user paused it
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
