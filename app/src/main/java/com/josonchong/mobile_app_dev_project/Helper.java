package com.josonchong.mobile_app_dev_project;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import es.dmoral.toasty.Toasty;

public class Helper {
    public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static FirebaseFirestore db = FirebaseFirestore.getInstance();

    //get current Date(), return with String format
    public static String getCurrentDateTime(){
        Date date = new Date();
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+11"));
        return sdf.format(date);
    }

    //convert zid to zmail
    public static String zidToZmail(String zid){
        return "z" + zid + "@ad.unsw.edu.au";
    }

    //convert zmail to zid
    public static String ZmailToZid(String zmail){
        String [] zmailSplit = zmail.split("@");
        zmailSplit = zmailSplit[0].split("z");
        System.out.println(zmailSplit[1]);
        return zmailSplit[1];
    }

    //Reference: https://stackoverflow.com/questions/8701634/send-email-intent
    //start email intent
    public static void sendEmail(Context context, String subject, String body){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("text/plain");
        intent.setData(Uri.parse("mailto:z5036635@student.unsw.edu.au"));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(intent, "Send Email"));
    }

    //add data to firestore asyn, used when onComplete is not needed
    public static void addDataToFirestoreAsyn(String collection, String documentID, String column, String value){
        Map<String, Object> data = new HashMap<>();
        data.put(column, value);
        db.collection(collection).document(documentID).update(data);
    }

    //determine if zid contains z or not, return zid without "z" anyway
    public static String getZidWithoutZ(String zid){
        if(zid.contains("z")){
            try {
                String[] zidSplit = zid.split("z");
                return zidSplit[zidSplit.length-1];
            }catch (Exception ex){
                return "";
            }
        }else{
            return zid;
        }
    }

    //search user with zid, start chat if zid exists
    public static void startChatWithZid(final Context context, final String zid){
        if(!zid.equals("")) {
            if (!zid.equals(CurrentSession.getCurrentUser().getZid())) {
                final ProgressDialog dialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_DARK);
                dialog.setMessage("Loading...");
                dialog.setCancelable(false);
                showDialog(dialog);
                FirebaseFirestore.getInstance().collection("users").document(zid)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot document = task.getResult();
                                    if (document.exists()) {
                                        User messagingWith = document.toObject(User.class);
                                        Intent intent = new Intent(context, MessageActivity.class);
                                        intent.putExtra("messagingWith", messagingWith);
                                        try {
                                            dismissDialog(dialog);
                                        }catch (Exception ex){

                                        }
                                        context.startActivity(intent);
                                    } else {
                                        try {
                                            dismissDialog(dialog);
                                        }catch (Exception ex){

                                        }
                                        Toasty.info(context, "Sorry, student " + zid + " don't have an account yet").show();
                                    }
                                } else {
                                    try {
                                        dismissDialog(dialog);
                                    }catch (Exception ex){

                                    }
                                    Toasty.info(context, "Sorry, student " + zid + " don't have an account yet").show();
                                }
                            }
                        });
            } else {
                Toasty.info(context, "Messaging yourself? Really?").show();
            }
        }else {
            Toasty.error(context, "Zid cannot be empty").show();
        }
    }

    //show dialog if it's not there
    public static void showDialog(Dialog dialog) {
        if(dialog!=null){
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    //dismiss dialog if it's there
    public static void dismissDialog(Dialog dialog) {
        if(dialog!=null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
