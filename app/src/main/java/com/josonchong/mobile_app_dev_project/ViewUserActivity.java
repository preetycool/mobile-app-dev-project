package com.josonchong.mobile_app_dev_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class ViewUserActivity extends AppCompatActivity {

    private TextView zidTV;
    private TextView majorTV;
    private TextView yearTV;
    private TextView genderTV;
    private CircleImageView imageView;
    private Button editBtn;
    private User viewingUser;
    private FloatingActionButton floatingActionButton;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dialog = new ProgressDialog(this, AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        zidTV = findViewById(R.id.zidTV);
        majorTV = findViewById(R.id.majorTV);
        yearTV = findViewById(R.id.yearTV);
        genderTV = findViewById(R.id.genderTV);
        editBtn = findViewById(R.id.editBtn);
        imageView = (CircleImageView) findViewById(R.id.imageView);
        floatingActionButton = findViewById(R.id.floatingActionButton);

        //set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        String zid = intent.getStringExtra("zid");

        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        showDialog();
        FirebaseFirestore.getInstance().collection("users").document(zid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                viewingUser = document.toObject(User.class);
                                if(viewingUser.getZid().equals(CurrentSession.getCurrentUser().getZid())){
                                    editBtn.setEnabled(true);
                                    floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.GREY)));
                                }else{
                                    floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.primary)));
                                    editBtn.setEnabled(false);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        editBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.GREY)));
                                    }else{
                                        editBtn.setBackgroundColor(getResources().getColor(R.color.GREY));
                                    }
                                    floatingActionButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Helper.startChatWithZid(ViewUserActivity.this, viewingUser.getZid());
                                        }
                                    });
                                }
                                zidTV.setText("z"+viewingUser.getZid() + "  " + viewingUser.getFirstName() + " " + viewingUser.getLastName());
                                majorTV.setText(viewingUser.getMajor());
                                yearTV.setText(viewingUser.getYear());
                                genderTV.setText(viewingUser.getGender());
                                if(viewingUser.getIconUrl()!=null){
                                    if(!viewingUser.getIconUrl().equals("")){
                                        try{
                                            Picasso.get().load(viewingUser.getIconUrl()).into(imageView);
                                        }catch (Exception ex){
                                            Picasso.get().load(R.mipmap.maleicon_round).into(imageView);
                                        }
                                    }else{
                                        Picasso.get().load(R.mipmap.maleicon_round).into(imageView);
                                    }
                                }else{
                                    Picasso.get().load(R.mipmap.maleicon_round).into(imageView);
                                }
                                dismissDialog();
                            } else {
                                dismissDialog();
                                Toasty.info(ViewUserActivity.this, "Sorry, user not found").show();
                            }
                        } else {
                            dismissDialog();
                            Toasty.info(ViewUserActivity.this, "Sorry, user not found").show();
                        }
                    }
                });



    }

    //bring user to AddUserDetailActivity to edit details
    public void editBtnOnClick(View view){
        Intent intent = new Intent(this, AddUserDetailActivity.class);
        intent.putExtra("editingUser", viewingUser);
        startActivity(intent);
        finish();
    }

    //show dialog if not showing
    private void showDialog() {
        try {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception ex) {

        }
    }

    //dismiss dialog if showing
    private void dismissDialog() {
        try {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception ex) {

        }
    }

}
