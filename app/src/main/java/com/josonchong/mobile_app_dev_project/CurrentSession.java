package com.josonchong.mobile_app_dev_project;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Date;

//this class stores everything in static, mainly current user information
public class CurrentSession {
    public static User currentUser;
    public static Date lastUpdate;

    //unused method
    public static User getCurrentUserWithMinimalInformation(){
        return new User(currentUser.getZid(), currentUser.getFirstName(), currentUser.getIconUrl());
    }

    //return current user
    public static User getCurrentUser() {
        return currentUser;
    }

    //set current user
    public static void setCurrentUser(User currentUser) {
        CurrentSession.currentUser = currentUser;
    }

    //unused method, mainly for testing purposes
    public static Date getLastUpdate() {
        return lastUpdate;
    }

    //unused method, mainly for testing purposes
    public static void setLastUpdate(Date lastUpdate) {
        CurrentSession.lastUpdate = lastUpdate;
    }

    //save settings into users device
    public static void setSetting(Context context, String column, boolean turnedOn){
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(column, turnedOn);
        editor.commit();

        Log.d("Settings", "setting: " + column + " is " + turnedOn);
    }

    //return user setting
    public static boolean getSetting(Context context, String column){
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
            return sharedPref.getBoolean(column, false);
    }
}
