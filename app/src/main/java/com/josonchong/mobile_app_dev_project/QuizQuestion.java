package com.josonchong.mobile_app_dev_project;

import java.io.Serializable;
import java.util.ArrayList;

public class QuizQuestion implements Serializable{
    private String question;
    private ArrayList<Option> options;
    private String explanations;

    public QuizQuestion(){

    }

    public QuizQuestion(String question, ArrayList<Option> options, String explanations) {
        this.question = question;
        this.options = options;
        this.explanations = explanations;
    }

    public String getExplanations() {
        return explanations;
    }

    public void setExplanations(String explanations) {
        this.explanations = explanations;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public ArrayList<Option> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<Option> options) {
        this.options = options;
    }

    public int getNumberOfAnswers(){
        int count = 0;
        for(int i = 0; i < options.size(); i ++){
            if(options.get(i).isAnswer()){
                count++;
            }
        }
        return count;
    }
}

class Option implements Serializable{
    private String option;
    private boolean isAnswer;

    public Option(){

    }

    public Option(String option, boolean isAnswer) {
        this.option = option;
        this.isAnswer = isAnswer;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public boolean isAnswer() {
        return isAnswer;
    }

    public void setAnswer(boolean answer) {
        isAnswer = answer;
    }
}