package com.josonchong.mobile_app_dev_project;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable{
    private User sender;
    private User receiver;
    private String messageText;
    private Date timeSent;
    private boolean isRead;

    public Message(){}


    public Message(User sender, User receiver, String messageText, Date timeSent) {
        this.sender = sender;
        this.receiver = receiver;
        this.messageText = messageText;
        this.timeSent = timeSent;
        this.isRead = false;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Date getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
