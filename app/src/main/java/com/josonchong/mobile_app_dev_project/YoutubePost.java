package com.josonchong.mobile_app_dev_project;

import java.io.Serializable;
import java.util.Date;

public class YoutubePost extends Post implements Serializable {
    private String url;

    YoutubePost(){
    }

    YoutubePost(User author, String postDate, String lastEdit, String title, String content, String url){
        super(author, postDate, lastEdit, title, content, "youtube");
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortenUrl(){
        if(url.contains("youtu.be")){
            String[] textSplit= url.split("/");
            return textSplit[textSplit.length-1];
        }else if(url.contains("watch?v=")){
            String[] textSplit= url.split("=");
            System.out.println("textSplit[textSplit.length-1]: " + textSplit[textSplit.length-1]);
            return textSplit[textSplit.length-1];
        }else{
            return url;
        }
    }
}
