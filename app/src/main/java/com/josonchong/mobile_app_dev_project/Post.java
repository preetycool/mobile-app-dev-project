package com.josonchong.mobile_app_dev_project;

import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Post implements Serializable{
    private String documentID;
    private User author;
    private String title;
    private String content;
    private String postDate;
    private String lastEdit;
    private int upVote;
    private int downVote;
    private String type = "discussion";
    private List<String> upVotedBy = new ArrayList<>();
    private List<String> downVotedBy = new ArrayList<>();


    Post(){
    }

    public Post(User author, String title, String content, String postDate, String lastEdit, int upVote, int downVote, String type) {
        this.author = author;
        this.title = title;
        this.content = content;
        this.postDate = postDate;
        this.lastEdit = lastEdit;
        this.upVote = upVote;
        this.downVote = downVote;
        this.type = type;
        this.upVote = 0;
        this.downVote = 0;
    }

    public Post(User author, String postDate, String lastEdit, String title, String content, String type) {
        this.author = author;
        this.postDate = postDate;
        this.lastEdit = lastEdit;
        this.title = title;
        this.content = content;
        this.upVote = 0;
        this.downVote = 0;
        this.type = type;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getLastEdit() {
        return lastEdit;
    }

    public void setLastEdit(String lastEdit) {
        this.lastEdit = lastEdit;
    }

    public int getUpVote() {
        return upVote;
    }

    public void setUpVote(int upVote) {
        this.upVote = upVote;
    }

    public int getDownVote() {
        return downVote;
    }

    public void setDownVote(int downVote) {
        this.downVote = downVote;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public List<String> getUpVotedBy() {
        return upVotedBy;
    }

    public void setUpVotedBy(List<String> upVotedBy) {
        this.upVotedBy = upVotedBy;
    }

    public List<String> getDownVotedBy() {
        return downVotedBy;
    }

    public void setDownVotedBy(List<String> downVotedBy) {
        this.downVotedBy = downVotedBy;
    }

    //like onClick, user's zid will be added into the post's upVote list in firestore
    public void updateUpVoteList(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> data = new HashMap<>();
        data.put("upVotedBy", upVotedBy);
        db.collection("posts").document(documentID).update(data);
    }

    //dislike onClick, user's zid will be added into the post's downVote list in firestore
    public void updateDownVoteList(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> data = new HashMap<>();
        data.put("downVotedBy", downVotedBy);
        db.collection("posts").document(documentID).update(data);
    }
}
