package com.josonchong.mobile_app_dev_project;

import java.io.Serializable;
import java.util.ArrayList;

public class QuizPost extends Post implements Serializable{
    private String topic;
    private ArrayList<QuizQuestion> quizQuestions;

    public QuizPost(){

    }

    public QuizPost(User author, String postDate, String lastEdit, String title, String content, String type, String topic) {
        super(author, postDate, lastEdit, title, content, type);
        this.topic = topic;
    }

    public QuizPost(User author, String postDate, String lastEdit, String title, String content, String type, String topic, ArrayList<QuizQuestion> quizQuestions) {
        super(author, postDate, lastEdit, title, content, type);
        this.topic = topic;
        this.quizQuestions = quizQuestions;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public ArrayList<QuizQuestion> getQuizQuestions() {
        return quizQuestions;
    }

    public void setQuizQuestions(ArrayList<QuizQuestion> quizQuestions) {
        this.quizQuestions = quizQuestions;
    }
}
