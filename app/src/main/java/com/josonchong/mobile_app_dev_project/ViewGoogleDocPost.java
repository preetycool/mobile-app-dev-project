package com.josonchong.mobile_app_dev_project;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class ViewGoogleDocPost extends YouTubeBaseActivity {
    private GoogleDocPost viewingPost;
    private ProgressDialog dialog;
    private ArrayList<Comment> commentArrayList = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private TextView title;
    private TextView time;
    private TextView fileNameTV;
    private TextView editableTV;
    private ImageView editableIV;
    private ImageView imageView2;
    private ExpandableTextView expandableTextView;
    private TextView author;
    private EditText textInputET;
    private Button likeBtn;
    private Button dislikeBtn;
    private Button reportBtn;
    private String TAG = "ViewGoogleDocPost";
    private String viewingPostDocumentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_google_doc_post);
        final Intent intent = getIntent();
        viewingPostDocumentID =  intent.getStringExtra("viewingPost");
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        title = findViewById(R.id.title);
        time = findViewById(R.id.time);
        fileNameTV = findViewById(R.id.fileNameTV);
        editableTV = findViewById(R.id.editableTV);
        editableIV = findViewById(R.id.editableIV);
        imageView2 = findViewById(R.id.imageView2);
        expandableTextView = (ExpandableTextView) findViewById(R.id.expand_text_view);
        author = findViewById(R.id.userEmailTV);
        textInputET = findViewById(R.id.textInputET);
        likeBtn = findViewById(R.id.likeBtn);
        dislikeBtn = findViewById(R.id.dislikeBtn);
        reportBtn = findViewById(R.id.reportBtn);

        //set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
}

    @Override
    protected void onResume() {
        addPostListener();
        addCommentsListener();
        super.onResume();
    }

    //add snapshot listener to the post
    public void addPostListener(){
        db.collection("posts").document(viewingPostDocumentID)
                .addSnapshotListener(ViewGoogleDocPost.this, MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (snapshot.getString("title") != null) {
                            viewingPost = snapshot.toObject(GoogleDocPost.class);
                            title.setText(viewingPost.getTitle());
                            time.setText("at " + viewingPost.getPostDate());
                            expandableTextView.setText(viewingPost.getContent());
                            author.setText("Post by: " + viewingPost.getAuthor().getFirstName());
                            author.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(ViewGoogleDocPost.this, ViewUserActivity.class);
                                    intent.putExtra("zid", viewingPost.getAuthor().getZid());
                                    startActivity(intent);
                                }
                            });
                            fileNameTV.setText(viewingPost.getDocumentName());
                            if(viewingPost.isEditable()){
                                Picasso.get().load(R.mipmap.tickicon).into(editableIV);
                                editableTV.setText("You are allowed to edit the document");
                            }else{
                                editableTV.setText("You are not allowed to edit the document");
                            }
                            fileNameTV.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    viewDoc(viewingPost.getUrl());
                                }
                            });
                            imageView2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    viewDoc(viewingPost.getUrl());
                                }
                            });
                            initBtns();
                        }
                    }
                });
    }

    //add snapshot listener to the comments
    public void addCommentsListener(){
        db.collection("posts").document(viewingPostDocumentID).collection("comments")
                .orderBy("datePosted", Query.Direction.DESCENDING)
                .addSnapshotListener(ViewGoogleDocPost.this, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        commentArrayList.clear();
                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.getString("comment") != null) {
                                Comment newComment = doc.toObject(Comment.class);
                                commentArrayList.add(newComment);
                            }
                        }
                        initRecycleView();
                    }
                });
    }

    //direct user to google doc with url
    private void viewDoc(String url){
        final String docUrl = url;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(docUrl));
        intent.setPackage("com.google.android.apps.docs.editors.docs");
        try{
            startActivity(intent);
        }catch (ActivityNotFoundException ex){
            System.out.println(ex);
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ViewGoogleDocPost.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Oops, seems like you don't have Google Docs installed in your device");
            alertBuilder.setMessage("Do you want to open the document with other applications?");
            alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent2 = new Intent(Intent.ACTION_VIEW);
                    intent2.setData(Uri.parse(docUrl));
                    startActivity(intent2);
            }
            });
            alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                } });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        }
    }

    //make comment on the post
    public void commentBtnClicked(View view){
        final String content = textInputET.getText().toString();
        if (!content.equals("")){
            textInputET.setText("");
            Comment newComment = new Comment(CurrentSession.currentUser.getZid(), content, Helper.getCurrentDateTime());
            ObjectMapper oMapper = new ObjectMapper();
            Map<String, Object> commentMap = oMapper.convertValue(newComment, Map.class);
            //showDialog();
            db.collection("posts").document(viewingPost.getDocumentID()).collection("comments")
                    //db.collection("posts/"+ viewingPost.getDocumentID() + "/comments")
                    .add(commentMap)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            Helper.addDataToFirestoreAsyn("posts", documentReference.getId(), "documentID", documentReference.getId());
                            Map<String, Object> data = new HashMap<>();
                            data.put("documentID", documentReference.getId());
                            db.collection("posts").document(viewingPost.getDocumentID()).collection("comments").document(documentReference.getId()).update(data);
                            Toasty.success(ViewGoogleDocPost.this, "Your comment has been created.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toasty.error(ViewGoogleDocPost.this, "Unexpected error occurred.", Toast.LENGTH_SHORT).show();
                            //dismissDialog();
                        }
                    });
        }else{
            Toasty.error(ViewGoogleDocPost.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //initialization comment recycler view
    private void initRecycleView(){
        RecyclerView recyclerView = findViewById(R.id.recycleView);
        CommentsRecycleViewAdapter adapter = new CommentsRecycleViewAdapter(this, commentArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    //send an email to report the post (bring up email intent)
    public void report(View view){
        Helper.sendEmail(this, "Reporting post " + viewingPost.getDocumentID(), "Title: \"" +viewingPost.getTitle() + "\"\n" + "Content: \"" +viewingPost.getContent() + "\"\n" + "Video Url: \"" +viewingPost.getUrl() + "\"\n\n" + "Reason(s) of reporting:\n ");
    }

    //like on click, upload the user's zid to the upvote list of the post
    public void likeBtnClicked(View view) {
        if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
            if (viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                viewingPost.getDownVotedBy().remove(CurrentSession.currentUser.getZid());
                viewingPost.updateDownVoteList();
            }
            viewingPost.getUpVotedBy().add(CurrentSession.currentUser.getZid());
        } else {
            viewingPost.getUpVotedBy().remove(CurrentSession.currentUser.getZid());
        }
        viewingPost.updateUpVoteList();
        initBtns();
    }

    //dislike on click, upload the user's zid to the downvote list of the post
    public void dislikeBtnClicked(View view) {
        if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
            if (viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
                viewingPost.getUpVotedBy().remove(CurrentSession.currentUser.getZid());
                viewingPost.updateUpVoteList();
            }
            viewingPost.getDownVotedBy().add(CurrentSession.currentUser.getZid());
        } else {
            viewingPost.getDownVotedBy().remove(CurrentSession.currentUser.getZid());
        }
        viewingPost.updateDownVoteList();
        initBtns();
    }

    //check version of user's phone, set like or dislike button's background/ background hint to dark grey if the user has liked/ disliked the post
    private void initBtns() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
                likeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.BLACK)));
            } else {
                likeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.GREEN)));
            }

            if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                dislikeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.BLACK)));
            } else {
                dislikeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            }
            likeBtn.setText("Like (" + viewingPost.getUpVotedBy().size() + ")");
            dislikeBtn.setText("dislike (" + viewingPost.getDownVotedBy().size() + ")");
        } else {
            if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {

                likeBtn.setBackgroundColor(getResources().getColor(R.color.GREY));

            } else {
                likeBtn.setBackgroundColor(getResources().getColor(R.color.DARKGREY));
            }

            if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                dislikeBtn.setBackgroundColor(getResources().getColor(R.color.GREY));
            } else {
                dislikeBtn.setBackgroundColor(getResources().getColor(R.color.DARKGREY));
            }

            likeBtn.setText("Like (" + viewingPost.getUpVotedBy().size() + ")");
            dislikeBtn.setText("dislike (" + viewingPost.getDownVotedBy().size() + ")");
        }
    }
}
