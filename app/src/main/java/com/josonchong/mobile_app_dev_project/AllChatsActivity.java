package com.josonchong.mobile_app_dev_project;

import android.content.Intent;
import android.net.nsd.NsdManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class AllChatsActivity extends AppCompatActivity {
    private FirebaseFirestore db;
    private String TAG = "AllChatsActivity";
    private ArrayList<String> chatZidArrayList = new ArrayList<>();
    private ArrayList<Chat> chatArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private EditText editText;
    private ArrayList<ListenerRegistration> listenerRegistrations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_chats);
        recyclerView = findViewById(R.id.recyclerView);
        editText = findViewById(R.id.editText);

        //the set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        db = FirebaseFirestore.getInstance();
        db.collection("messages").document(CurrentSession.getCurrentUser().getZid())
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {

                            return;
                        }

                        if (snapshot != null && snapshot.exists()) {
                            Log.d(TAG, "Current data: " + snapshot.getData());
                            if (snapshot.get("conversations") != null)
                                chatZidArrayList = (ArrayList<String>) snapshot.get("conversations");
                            addChatsSnapshotListeners();
                        } else {
                            Log.d(TAG, "Current data: null");
                        }
                    }
                });
    }

    //add snapshot listener for all chats
    private void addChatsSnapshotListeners(){
        Log.d(TAG, "chatZidArrayList.toString(): " + chatZidArrayList.toString());
        chatArrayList.clear();
        listenerRegistrations.clear();
        for(int i = 0; i < chatZidArrayList.size(); i ++){
            final int num = i;
            ListenerRegistration listenerRegistration = db.collection("messages").document(CurrentSession.getCurrentUser().getZid()).collection(chatZidArrayList.get(num)).document("lastMessage")
                    .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                        @Override
                        public void onEvent(@Nullable final DocumentSnapshot snapshot,
                                            @Nullable FirebaseFirestoreException e) {
                            if (e != null) {
                                Log.d(TAG, "FirebaseFirestoreException: " + e);
                                return;
                            }


                            if (snapshot != null && snapshot.exists()) {
                                    db.collection("users").document(chatZidArrayList.get(num))
                                            .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                             @Override
                                             public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    System.out.println("successful");
                                                        DocumentSnapshot document = task.getResult();
                                                        if (document.exists()) {
                                                            User user = document.toObject(User.class);
                                                            Chat chat = new Chat(user, snapshot.getString("lastMessage"), snapshot.getDate("timeSent"));
                                                            boolean isExist = false;
                                                            for(int i = 0; i < chatArrayList.size(); i++){
                                                                if(chatArrayList.get(i).getUser().getZid().equals(user.getZid())){
                                                                    chatArrayList.set(i, chat);
                                                                    isExist = true;
                                                                }
                                                            }
                                                            if(!isExist){
                                                                chatArrayList.add(chat);
                                                            }
                                                            initRecycleView();
                                                        } else {
                                                            Log.d(TAG, "Document Current data: " + document.getData());
                                                        }
                                                    } else {
                                                        Log.d(TAG, "!task.isSuccessful() ");
                                                    }
                                                }
                                            });
                                Log.d(TAG, "Current data: " + snapshot.getData());
                            } else {
                                Log.d(TAG, "Current data: null");
                            }
                        }
                    });
            listenerRegistrations.add(listenerRegistration);
        }

    }

    //initialization recycler view for chats
    private void initRecycleView(){
        ChatsRecycleViewAdapter adapter = new ChatsRecycleViewAdapter(this, chatArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    //start chat with user using zid
    public void searchUser(View view){
        final String zid = Helper.getZidWithoutZ(editText.getText().toString());
        Helper.startChatWithZid(this, zid);
    }


}
