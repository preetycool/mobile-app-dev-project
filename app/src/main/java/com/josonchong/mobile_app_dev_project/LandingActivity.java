package com.josonchong.mobile_app_dev_project;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import io.grpc.InternalNotifyOnServerBuild;

import static android.widget.Toast.LENGTH_SHORT;


public class LandingActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MenuItem.OnMenuItemClickListener {

    private ArrayList<Post> postArrayList = new ArrayList<>();
    private ArrayList<Post> filteredPostArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    //private Button createBtn;
    private NavigationView nav_view;
    private TextView id;
    private TextView userIDTV;
    private TextView userEmailTV;
    private CircleImageView imageView;
    private ProgressDialog dialog;
    private ArrayList<ListenerRegistration> listenerRegistrations = new ArrayList<>();
    private ListenerRegistration postListenerRegistration;
    //private FloatingActionButton youtubefab, googleDocfab, quizfab, discussionfab, questionfab;
    private CircleImageView quizImageView, questionImageView, youtubeImageView, googledocsImageView, discussionPostsImageView;
    private View viewOnClick;
    private boolean filterOn = false;




    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final String TAG = "LandingActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        listenerRegistrations.clear();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.recycleView);
        //createBtn = findViewById(R.id.createBtn);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        id = findViewById(R.id.id);
        id.setText("Hello " + CurrentSession.currentUser.getFirstName()+ "!");
        //spinner1 = findViewById(R.id.spinner1);
        this.setTitle("Home");

        nav_view = findViewById(R.id.nav_view);
        View headerView = nav_view.getHeaderView(0);
        userIDTV = headerView.findViewById(R.id.userIDTV);
        userEmailTV = headerView.findViewById(R.id.userEmailTV);
        imageView = headerView.findViewById(R.id.imageView);


        if (CurrentSession.currentUser.getIconUrl() != null) {
            try{
                Picasso.get().load(CurrentSession.currentUser.getIconUrl()).into(imageView);
            }catch (Exception ex){
                loadMorFIcon(CurrentSession.currentUser, imageView);
            }
        } else {
            loadMorFIcon(CurrentSession.currentUser, imageView);
        }

        userIDTV.setText("z"+CurrentSession.currentUser.getZid());
        userEmailTV.setText(CurrentSession.currentUser.getFirstName() + " " + CurrentSession.currentUser.getLastName());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        addUpdateListener();

        //set onClick listeners for 5 image views
        quizImageView = (CircleImageView) findViewById(R.id.quizImageView);
        quizImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewOnClick != null){
                    if(viewOnClick != v){
                        scaleDown(viewOnClick);
                        scaleUp(v);
                        filterArrayList("quiz");
                        filterOn = true;
                        viewOnClick = v;
                    }
                    else{
                        scaleDown(v);
                        filteredPostArrayList = postArrayList;
                        filterOn = false;
                        viewOnClick = null;
                    }
                }else{
                    scaleUp(v);
                    filterArrayList("quiz");
                    filterOn = true;
                    viewOnClick = v;
                }
                initRecycleView();
            }
        });

        questionImageView = (CircleImageView) findViewById(R.id.questionImageView);
        questionImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewOnClick != null){
                    if(viewOnClick != v){
                        scaleDown(viewOnClick);
                        scaleUp(v);
                        filterArrayList("question");
                        filterOn = true;
                        viewOnClick = v;
                    }
                    else{
                        scaleDown(v);
                        filteredPostArrayList = postArrayList;
                        filterOn = false;
                        viewOnClick = null;
                    }
                }else{
                    scaleUp(v);
                    filterArrayList("question");
                    filterOn = true;
                    viewOnClick = v;
                }
                initRecycleView();
            }
        });

        youtubeImageView = (CircleImageView) findViewById(R.id.youtubeImageView);
        youtubeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewOnClick != null){
                    if(viewOnClick != v){
                        scaleDown(viewOnClick);
                        scaleUp(v);
                        filterArrayList("youtube");
                        filterOn = true;
                        viewOnClick = v;
                    }
                    else{
                        scaleDown(v);
                        filteredPostArrayList = postArrayList;
                        filterOn = false;
                        viewOnClick = null;
                    }
                }else{
                    scaleUp(v);
                    filterArrayList("youtube");
                    filterOn = true;
                    viewOnClick = v;
                }
                initRecycleView();
            }
        });

        googledocsImageView = (CircleImageView) findViewById(R.id.googledocsImageView);
        googledocsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewOnClick != null){
                    if(viewOnClick != v){
                        scaleDown(viewOnClick);
                        scaleUp(v);
                        filterArrayList("googleDoc");
                        filterOn = true;
                        viewOnClick = v;
                    }
                    else{
                        scaleDown(v);
                        filteredPostArrayList = postArrayList;
                        filterOn = false;
                        viewOnClick = null;
                    }
                }else{
                    scaleUp(v);
                    filterArrayList("googleDoc");
                    filterOn = true;
                    viewOnClick = v;
                }
                initRecycleView();
            }
        });

        discussionPostsImageView = (CircleImageView) findViewById(R.id.discussionPostsImageView);
        discussionPostsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewOnClick != null){
                    if(viewOnClick != v){
                        scaleDown(viewOnClick);
                        scaleUp(v);
                        filterArrayList("discussion");
                        filterOn = true;
                        viewOnClick = v;
                    }
                    else{
                        scaleDown(v);
                        filteredPostArrayList = postArrayList;
                        filterOn = false;
                        viewOnClick = null;
                    }
                }else{
                    scaleUp(v);
                    filterArrayList("discussion");
                    viewOnClick = v;
                }
                initRecycleView();
            }
        });


        System.out.println("CurrentSession.getSetting(this, \"newPostNotifications\" " + CurrentSession.getSetting(this, "newPostNotifications"));
        if(CurrentSession.getSetting(this, "newPostNotifications")){
            addPostsSnapshotListeners(db.collection("posts"));
        }else{
            try {
                postListenerRegistration.remove();
            }catch (Exception ex){

            }
        }

    }

    //Reference: https://stackoverflow.com/questions/33916287/android-scale-image-view-with-animation
    //make image view to normal size with animation
    private void scaleDown(View v){
        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(ObjectAnimator.ofFloat(v, "scaleX", 1f).setDuration(1000))
                .with(ObjectAnimator.ofFloat(v, "scaleY", 1f).setDuration(1000));
        scaleDown.start();
    }

    //make image view larger  with animation
    private void scaleUp(View v){
        AnimatorSet scaleUp = new AnimatorSet();
        scaleUp.play(ObjectAnimator.ofFloat(v, "scaleX", 1.4f).setDuration(1000))
                .with(ObjectAnimator.ofFloat(v, "scaleY", 1.4f).setDuration(1000));
        scaleUp.start();
    }

    //Reference: https://stackoverflow.com/questions/15454995/popupmenu-with-icons
    @SuppressLint("RestrictedApi")
    private void showCreatePostDropDown(View view){
        Log.d(TAG, "showCreatePostDropDown called");
        MenuBuilder menuBuilder =new MenuBuilder(this);
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.create_post_menu, menuBuilder);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, view);
        optionsMenu.setForceShowIcon(true);

        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.youtubeDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateYoutubePostActivity.class));
                        return true;
                    case R.id.googleDocDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateGoogleDocPostActivity.class));
                        return true;
                    case R.id.discussionDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateDiscussionPostActivity.class));
                        return true;
                    case R.id.questionDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateQuestionPostActivity.class));
                        return true;
                    case R.id.quizDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateQuizPostActivity.class));
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {}
        });

        optionsMenu.show();
    }

    //create menu backup
    /*
    PopupMenu dropDownMenu = new PopupMenu(LandingActivity.this, view);
        dropDownMenu.getMenuInflater().inflate(R.menu.create_post_menu, dropDownMenu.getMenu());
        dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.youtubeDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateYoutubePostActivity.class));
                        break;
                    case R.id.googleDocDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateGoogleDocPostActivity.class));
                        break;
                    case R.id.discussionDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateDiscussionPostActivity.class));
                        break;
                    case R.id.questionDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateQuestionPostActivity.class));
                        break;
                    case R.id.quizDropdown:
                        startActivity(new Intent(LandingActivity.this, CreateQuizPostActivity.class));
                        break;

                }

                return true;
            }
        });
        dropDownMenu.show();
     */

    //filter posts spinner backup
//        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                if(postArrayList.size()>0) {
//                    Log.d(TAG, "spinner position: " + position);
//                    switch (position) {
//                        case 0:
//                            filteredPostArrayList = postArrayList;
//                            break;
//                        case 1:
//                            filterArrayList("youtube");
//                            break;
//                        case 2:
//                            filterArrayList("googleDoc");
//                            break;
//                        case 3:
//                            filterArrayList("quiz");
//                            break;
//                        case 4:
//                            filterArrayList("discussion");
//                            break;
//                        case 5:
//                            filterArrayList("question");
//                            break;
//                    }
//                }
//                initRecycleView();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//            }
//
//        });



    //remove change listeners
    private void removeAllCommentListenerRegistrations(){
        for(int i = 0; i < listenerRegistrations.size(); i ++){
            try {
                listenerRegistrations.get(i).remove();
            }catch (Exception ex){

            }
        }
    }

    //add UpdateListener for posts when resume
    @Override
    protected void onResume() {
        addUpdateListener();
        super.onResume();
    }

    //remove all ListenerRegistrations when the activity ends
    @Override
    protected void onDestroy() {
        try {
            postListenerRegistration.remove();
        }catch (Exception ex){

        }
        removeAllCommentListenerRegistrations();

        super.onDestroy();
    }


    //load icon depends on gender, should only be called when user don't have an icon
    private void loadMorFIcon(User user, CircleImageView circleImageView){
        if (user.getGender().equals("M")){
            Picasso.get().load(R.mipmap.maleicon_round).into(circleImageView);
        }else{
            Picasso.get().load(R.mipmap.femaleicon_round).into(circleImageView);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }

    //override on backpressed, users can only logout with the logout button
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    //Override onCreateOptionsMenu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.landing, menu);
        return true;
    }


    //set onOptionsItemSelected for the items in the toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.plusbutton) {
            showCreatePostDropDown(findViewById(R.id.plusbutton));
           return true;
        }else if(id == R.id.helpbutton){
            startActivity(new Intent(this, HelpActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    test messaging
                Intent intent = new Intent(this, MessageActivity.class);
            if(CurrentSession.getCurrentUser().getZid().equals("5036635")){
                intent.putExtra("messagingWith", new User("5021118", "Preet", ""));
            }else{
                intent.putExtra("messagingWith", new User("5036635", "Joson", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzP_jSwEIK0R2AL81djMJKhP2s1wBUZFJN_oEPrxyHKApShYNd9A"));
            }
            startActivity(intent);
     */

    //left nav drawer options
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_messages:
                startActivity(new Intent(this, AllChatsActivity.class));
                break;
            case R.id.nav_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.home:
                startActivity(new Intent(this, LandingActivity.class));
                break;
            case R.id.nav_logout:
                //sign out firebase user and clearing all activities opened
                FirebaseAuth.getInstance().signOut();
                finishAffinity();
                startActivity(new Intent(LandingActivity.this, LoginActivity.class));
                break;
            case R.id.nav_profile:
                Intent intent = new Intent(LandingActivity.this, ViewUserActivity.class);
                intent.putExtra("zid", CurrentSession.getCurrentUser().getZid());
                startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*
        SignOut backup
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
         */

    private void initRecycleView() {
        System.out.println("init");
        recyclerView = findViewById(R.id.recycleView);
        PostsRecycleViewAdapter adapter = new PostsRecycleViewAdapter(this, filteredPostArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    //dismiss dialog if showing
    private void dismissDialog() {
        try {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }catch (Exception ex){

        }
    }

    //listen to posts updates
    public void addUpdateListener(){
        db.collection("posts").orderBy("postDate", Query.Direction.DESCENDING)
                .addSnapshotListener(LandingActivity.this, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        removeAllCommentListenerRegistrations();
                        postArrayList.clear();
                        listenerRegistrations.clear();
                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.getString("title") != null) {
                                Post newPost = doc.toObject(Post.class);
                                postArrayList.add(newPost);
                                if(newPost.getAuthor().getZid().equals(CurrentSession.getCurrentUser().getZid())){
                                    if(CurrentSession.getSetting(LandingActivity.this, "newCommentNotifications")){
                                        Query query = db.collection("posts").document(newPost.getDocumentID()).collection("comments");
                                        addCommentsSnapshotListeners(query);
                                    }
                                }
                            }
                        }
                        if(filteredPostArrayList.size()==0) {
                            filteredPostArrayList = postArrayList;
                        }
                        initRecycleView();
                    }
                });
    }

    //listen to comments for a specific post, should only be invoked with users turn on the option in settings
    private void addCommentsSnapshotListeners(Query query){
        ListenerRegistration listenerRegistration = query.addSnapshotListener(MetadataChanges.EXCLUDE, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.get("authorID") != null) {
                                final Comment comment = doc.toObject(Comment.class);
                                if(!comment.getAuthorID().equals(CurrentSession.currentUser.getZid())){
                                try{
                                    if(comment.getDatePosted().equals(Helper.getCurrentDateTime())){
                                        DocumentReference docRef = db.collection("users").document(comment.getAuthorID());
                                        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    DocumentSnapshot document = task.getResult();
                                                    if (document != null && document.getString("zid")!=null) {
                                                        showNotification(document.getString("firstName")+" has just commented on your post", "\""+comment.getComment()+"\"", 1, null);
                                                    } else {
                                                        Log.d(TAG, "No such document");
                                                    }
                                                } else {
                                                    Log.d(TAG, "User not found");
                                                }
                                            }

                                        });
                                    }
                                }catch (Exception ex){

                                }
                                }
                            }
                        }
                        //CurrentSession.setLastUpdate(new Date());
                    }
                });
        listenerRegistrations.add(listenerRegistration);
        Log.d(TAG, "listenerRegistrations.size()): " + listenerRegistrations.size());

    }

    //listen to posts updates, should only be invoked with users turn on the option in settings
    private void addPostsSnapshotListeners(Query query){
        ListenerRegistration listenerRegistration = query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                for (QueryDocumentSnapshot doc : value) {
                    if (doc.get("title") != null) {
                        Post post = doc.toObject(Post.class);
                        if(!post.getAuthor().getZid().equals(CurrentSession.currentUser.getZid())){
                            if(post.getLastEdit().equals(Helper.getCurrentDateTime())) {
                                showNotification(post.getAuthor().getFirstName() + " has just created a new post", "\"" + post.getTitle() + "\"", 0, null);
                            }
                        }
                    }
                }
                //CurrentSession.setLastUpdate(new Date());
            }
        });
        postListenerRegistration = listenerRegistration;
        }

        //if type == type, the post is added into the filteredPostArrayList, used for filtering posts
        private void filterArrayList(String type){
        filteredPostArrayList = new ArrayList<>();
            for(int i = 0; i < postArrayList.size(); i ++){
                if(postArrayList.get(i).getType().equals(type)){
                    filteredPostArrayList.add(postArrayList.get(i));
                }
            }
        }


//        //check which fab is clicked, direct users to corresponding create post activity
//        public void fabsOnclick(View view){
//            switch (view.getId()){
//                case R.id.youtubefab:
//                    startActivity(new Intent(LandingActivity.this, CreateYoutubePostActivity.class));
//                    break;
//                case R.id.googleDocfab:
//                    startActivity(new Intent(LandingActivity.this, CreateGoogleDocPostActivity.class));
//                    break;
//                case R.id.discussionfab:
//                    startActivity(new Intent(LandingActivity.this, CreateDiscussionPostActivity.class));
//                    break;
//                case R.id.questionfab:
//                    startActivity(new Intent(LandingActivity.this, CreateQuestionPostActivity.class));
//                    break;
//                case R.id.quizfab:
//                    startActivity(new Intent(LandingActivity.this, CreateQuizPostActivity.class));
//                    break;
//            }
//        }



    public void testingRealTimeDataListener(){
        String documentID = "4INoauX9U6LCxP7UOq3k";
        db.collection("posts")
                .document(documentID).collection("comments")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.get("authorID") != null) {
                                Comment comment = doc.toObject(Comment.class);
                                //if(!comment.getAuthorID().equals(CurrentSession.currentUser.getZid())){
                                    try{
                                        if(Helper.sdf.parse(comment.getDatePosted()).after(CurrentSession.getLastUpdate())){
                                            showNotification("update", comment.getComment(), 1, null);
                                        }
                                    }catch (Exception ex){

                                    }
                                //}
                            }
                        }
                        CurrentSession.setLastUpdate(new Date());
                    }
                });
    }

    //show notification, including pending intent will bring users to that intent when clicked. For now, we leave it as null.
    private void showNotification(String title, String content, int id, @Nullable PendingIntent pendingIntent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,"default")
                .setSmallIcon(R.mipmap.master_java_icon)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        if(pendingIntent!=null){
            mBuilder.setContentIntent(pendingIntent);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        notificationManager.notify("default", id, mBuilder.build());
    }

    //do not use this
    private void updatePosts() {
        postArrayList.clear();
        dialog.show();
        db.collection("posts")
                .orderBy("postDate", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            try {
                                List<Post> posts = task.getResult().toObjects(Post.class);
                                postArrayList.addAll(posts);
                                initRecycleView();
                                dismissDialog();
                            } catch (Exception ex) {
                                System.out.println("Exception: " + ex);
                                Toasty.error(LandingActivity.this, "Sorry, expected error occurred when loading posts. Please contact customer support or try again later.", Toast.LENGTH_LONG).show();
                                dismissDialog();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                            dismissDialog();
                        }
                    }
                });
    }
}
