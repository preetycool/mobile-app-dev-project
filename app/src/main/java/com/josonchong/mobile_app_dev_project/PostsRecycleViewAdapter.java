package com.josonchong.mobile_app_dev_project;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class PostsRecycleViewAdapter extends RecyclerView.Adapter<PostsRecycleViewAdapter.ViewHolder> {

    private ArrayList<Post> postArrayList = new ArrayList<>();
    private Context mContext;
    private final String TAG = "PostsRecycleViewAd";


    public PostsRecycleViewAdapter(Context context, ArrayList<Post> postArraylist) {
        this.postArrayList = postArraylist;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_posts, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        //load icon into the recycler view depends on the post's type
        final Post bindPost = postArrayList.get(i);
        if (bindPost.getType().equals("youtube")) {
            viewHolder.title.setText("(YouTube) ");
            viewHolder.image.setImageResource(R.drawable.youtubesymbol);
        } else if (bindPost.getType().equals("googleDoc")) {
            viewHolder.title.setText("(Google Doc) ");
            viewHolder.image.setImageResource(R.drawable.docs);
        } else if (bindPost.getType().equals("discussion")) {
            viewHolder.title.setText("(Discussion) ");
            viewHolder.image.setImageResource(R.drawable.discussion_final);
        } else if (bindPost.getType().equals("question")) {
            viewHolder.title.setText("(Question) ");
            viewHolder.image.setImageResource(R.drawable.questionforrecycler);
        } else if (bindPost.getType().equals("quiz")) {
            viewHolder.title.setText("(Quiz) ");
            viewHolder.image.setImageResource(R.drawable.testicon);
        }
        viewHolder.title.append(bindPost.getTitle());

        //start activity depends on the post's type
        viewHolder.posts_parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bindPost.getType().equals("youtube")) {
                    startActivity(bindPost, ViewYoutubePost.class);
                } else if (bindPost.getType().equals("googleDoc")) {
                    startActivity(bindPost, ViewGoogleDocPost.class);
                } else if (bindPost.getType().equals("discussion")) {
                    startActivity(bindPost, ViewDiscussionPost.class);
                } else if (bindPost.getType().equals("question")) {
                    startActivity(bindPost, ViewQuestionPost.class);
                } else if (bindPost.getType().equals("quiz")) {
                    startActivity(bindPost, ViewQuizPost.class);
                }
            }
        });
    }

    //start activity with class and post
    private void startActivity(Post bindPost, Class postClass) {
        Intent intent = new Intent(mContext, postClass);
        intent.putExtra("viewingPost", bindPost.getDocumentID());
        mContext.startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return postArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        //TextView author;
        //TextView datePosted;
        TextView title;
        RelativeLayout posts_parent_layout;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageView);
            //author = itemView.findViewById(R.id.userEmailTV);
            //datePosted = itemView.findViewById(R.id.datePosted);
            title = itemView.findViewById(R.id.title);
            posts_parent_layout = itemView.findViewById(R.id.posts_parent_layout);
        }
    }
}

/*Backup of startYT
private void startYTActivity(Post bindPost){
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("posts").document(bindPost.getDocumentID());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        YoutubePost youtubePost = task.getResult().toObject(YoutubePost.class);
                        System.out.println("youtubePost.getUrl(): " + youtubePost.getUrl());
                        Intent intent = new Intent(mContext, ViewYoutubePost.class);
                        intent.putExtra("viewingPost", youtubePost.getDocumentID());
                        mContext.startActivity(intent);
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }
 */