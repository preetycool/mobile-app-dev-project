package com.josonchong.mobile_app_dev_project;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

import es.dmoral.toasty.Toasty;

public class CreateDiscussionPostActivity extends YouTubeBaseActivity {
    private EditText titleET;
    private EditText contentET;
    private EditText topicET;
    private ImageView crossIV;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final String TAG = "CreateDisPostActivity";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_discussion_post);
        titleET = findViewById(R.id.titleET);
        contentET = findViewById(R.id.contentET);
        topicET = findViewById(R.id.topicET);
        crossIV = findViewById(R.id.crossIV);
        dialog = new ProgressDialog(this, android.app.AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leave();
            }
        });

    }

    //upload post to Firestore
    public void submit(View view){
        String title = titleET.getText().toString();
        String content = contentET.getText().toString();
        if(!topicET.getText().toString().equals("")){
            title = "[" + topicET.getText().toString() + "] " + title;
        }
        if (!title.equals("")&&!content.equals("")){
            Post newPost = new Post(CurrentSession.currentUser, Helper.getCurrentDateTime(), Helper.getCurrentDateTime(), title, content, "discussion");
                ObjectMapper oMapper = new ObjectMapper();
                Map<String, Object> postMap = oMapper.convertValue(newPost, Map.class);
                //.System.out.println(youtubePostMap);
                dialog.show();
                db.collection("posts")
                        .add(postMap)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                Helper.addDataToFirestoreAsyn("posts", documentReference.getId(), "documentID", documentReference.getId());
                                Toasty.success(CreateDiscussionPostActivity.this, "Your post has been created.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toasty.error(CreateDiscussionPostActivity.this, "Unexpected error occurred.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        });
        }else{
            Toasty.error(CreateDiscussionPostActivity.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //dismiss dialog if it is showing
    private void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    //override onBackPressed
    @Override
    public void onBackPressed(){
        leave();
    }

    //Confirm if user wants to leave this page
    private void leave(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(CreateDiscussionPostActivity.this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Do you really want to leave this page?");
        alertBuilder.setMessage("Your changes will be discarded.");
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            } });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }
}
