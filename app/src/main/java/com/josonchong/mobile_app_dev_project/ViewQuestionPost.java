package com.josonchong.mobile_app_dev_project;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class ViewQuestionPost extends YouTubeBaseActivity {
    private QuestionPost viewingPost;
    private ProgressDialog dialog;
    private ArrayList<Comment> commentArrayList = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private TextView title;
    private TextView time;
    private TextView solutionTV;
    private ConstraintLayout constraintLayout5;
    private ExpandableTextView expandableTextView;
    private ExpandableTextView expandableTextView2;
    private TextView author;
    private EditText textInputET;
    private Button likeBtn;
    private Button dislikeBtn;
    private Button reportBtn;
    String viewingPostDocumentID;
    private String TAG = "ViewQuestionPost";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_question_post);
        final Intent intent = getIntent();
        viewingPostDocumentID = intent.getStringExtra("viewingPost");
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        title = findViewById(R.id.title);
        time = findViewById(R.id.time);
        expandableTextView = (ExpandableTextView) findViewById(R.id.expand_text_view);
        expandableTextView2 = (ExpandableTextView) findViewById(R.id.expand_text_view2);
        author = findViewById(R.id.userEmailTV);
        solutionTV = findViewById(R.id.solutionTV);
        constraintLayout5 = findViewById(R.id.constraintLayout5);
        textInputET = findViewById(R.id.textInputET);
        likeBtn = findViewById(R.id.likeBtn);
        dislikeBtn = findViewById(R.id.dislikeBtn);
        reportBtn = findViewById(R.id.reportBtn);

        //set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /*
        TextView textView1 = expandableTextView.findViewById(R.id.expandable_text);
        TextView textView2 = findViewById(R.id.expandable_text);
        Linkify.addLinks(textView1, Linkify.WEB_URLS);
        Linkify.addLinks(textView2, Linkify.WEB_URLS | Linkify.EMAIL_ADDRESSES);
        */

    }

    @Override
    protected void onResume() {
        addPostListener();
        addCommentsListener();
        super.onResume();
    }

    //add snapshot listener to the post
    public void addPostListener() {
        db.collection("posts").document(viewingPostDocumentID)
                .addSnapshotListener(ViewQuestionPost.this, MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (snapshot.getString("title") != null) {
                            viewingPost = snapshot.toObject(QuestionPost.class);
                            title.setText(viewingPost.getTitle());
                            time.setText("at " + viewingPost.getPostDate());

                            author.setText("Post by: " + viewingPost.getAuthor().getFirstName());
                            author.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(ViewQuestionPost.this, ViewUserActivity.class);
                                    intent.putExtra("zid", viewingPost.getAuthor().getZid());
                                    startActivity(intent);
                                }
                            });
                            if (viewingPost.getSolution() != null) {
                                expandableTextView.setText("[Solved] " + viewingPost.getContent());
                                setAuthor();
                                expandableTextView2.setText(viewingPost.getSolution().getComment());
                                constraintLayout5.setVisibility(View.VISIBLE);
                            } else {
                                expandableTextView.setText("[Unresolved] " + viewingPost.getContent());
                                constraintLayout5.setVisibility(View.GONE);
                            }
                            initBtns();

                        }
                    }
                });
    }

    //add snapshot listener to the comments
    public void addCommentsListener() {
        db.collection("posts").document(viewingPostDocumentID).collection("comments")
                .orderBy("datePosted", Query.Direction.DESCENDING)
                .addSnapshotListener(ViewQuestionPost.this, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        commentArrayList.clear();
                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.getString("comment") != null) {
                                Comment newComment = doc.toObject(Comment.class);
                                commentArrayList.add(newComment);
                            }
                        }
                        initRecycleView();
                    }
                });
    }

    //load solution if exist
    private void setAuthor() {
        db.collection("users").document(viewingPost.getSolution().getAuthorID())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document != null) {
                                solutionTV.setText("Solution: posted by " + document.getString("firstName"));
                                solutionTV.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(ViewQuestionPost.this, ViewUserActivity.class);
                                        intent.putExtra("zid", viewingPost.getSolution().getAuthorID());
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                Log.d(TAG, "No such document");
                            }
                        } else {
                            Log.d(TAG, "get failed with ", task.getException());
                            dismissDialog();
                        }
                    }
                });
    }

    //make comment on the post
    public void commentBtnClicked(View view) {
        final String content = textInputET.getText().toString();
        if (!content.equals("")) {
            textInputET.setText("");
            Comment newComment = new Comment(CurrentSession.currentUser.getZid(), content, Helper.getCurrentDateTime());
            ObjectMapper oMapper = new ObjectMapper();
            Map<String, Object> commentMap = oMapper.convertValue(newComment, Map.class);
            db.collection("posts").document(viewingPost.getDocumentID()).collection("comments")

                    .add(commentMap)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            Helper.addDataToFirestoreAsyn("posts", documentReference.getId(), "documentID", documentReference.getId());
                            Map<String, Object> data = new HashMap<>();
                            data.put("documentID", documentReference.getId());
                            db.collection("posts").document(viewingPost.getDocumentID()).collection("comments").document(documentReference.getId()).update(data);
                            Toasty.success(ViewQuestionPost.this, "Your comment has been created.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toasty.error(ViewQuestionPost.this, "Unexpected error occurred.", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Toasty.error(ViewQuestionPost.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //initialization comment recycler view
    private void initRecycleView() {
        RecyclerView recyclerView = findViewById(R.id.recycleView);
        CommentsRecycleViewAdapter adapter = new CommentsRecycleViewAdapter(this, commentArrayList, viewingPost, ViewQuestionPost.this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    //send an email to report the post (bring up email intent)
    public void report(View view) {
        Helper.sendEmail(this, "Reporting post " + viewingPost.getDocumentID(), "Title: \"" + viewingPost.getTitle() + "\"\n" + "Content: \"" + viewingPost.getContent() + "\"\n\n" + "Reason(s) of reporting:\n ");
    }

    //like on click, upload the user's zid to the upvote list of the post
    public void likeBtnClicked(View view) {
        if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
            if (viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                viewingPost.getDownVotedBy().remove(CurrentSession.currentUser.getZid());
                viewingPost.updateDownVoteList();
            }
            viewingPost.getUpVotedBy().add(CurrentSession.currentUser.getZid());
        } else {
            viewingPost.getUpVotedBy().remove(CurrentSession.currentUser.getZid());
        }
        viewingPost.updateUpVoteList();
        initBtns();
    }

    //dislike on click, upload the user's zid to the downvote list of the post
    public void dislikeBtnClicked(View view) {
        if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
            if (viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
                viewingPost.getUpVotedBy().remove(CurrentSession.currentUser.getZid());
                viewingPost.updateUpVoteList();
            }
            viewingPost.getDownVotedBy().add(CurrentSession.currentUser.getZid());
        } else {
            viewingPost.getDownVotedBy().remove(CurrentSession.currentUser.getZid());
        }
        viewingPost.updateDownVoteList();
        initBtns();
    }

    //check version of user's phone, set like or dislike button's background/ background hint to dark grey if the user has liked/ disliked the post
    private void initBtns() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
                likeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.BLACK)));
            } else {
                likeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.GREEN)));
            }

            if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                dislikeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.BLACK)));
            } else {
                dislikeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            }
            likeBtn.setText("Like (" + viewingPost.getUpVotedBy().size() + ")");
            dislikeBtn.setText("dislike (" + viewingPost.getDownVotedBy().size() + ")");
        } else {
            if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {

                likeBtn.setBackgroundColor(getResources().getColor(R.color.GREY));

            } else {
                likeBtn.setBackgroundColor(getResources().getColor(R.color.DARKGREY));
            }

            if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                dislikeBtn.setBackgroundColor(getResources().getColor(R.color.GREY));
            } else {
                dislikeBtn.setBackgroundColor(getResources().getColor(R.color.DARKGREY));
            }

            likeBtn.setText("Like (" + viewingPost.getUpVotedBy().size() + ")");
            dislikeBtn.setText("dislike (" + viewingPost.getDownVotedBy().size() + ")");
        }
    }

    //show dialog if not showing
    private void showDialog() {
        try {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception ex) {

        }
    }

    //dismiss dialog if showing
    private void dismissDialog() {
        try {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception ex) {

        }
    }
}
