package com.josonchong.mobile_app_dev_project;

import java.io.Serializable;
import java.util.Date;

public class Chat implements Serializable{
    private String lastMessage;
    private Date timeSent;
    private User user;

    public Chat(User user, String lastMessage, Date timeSent) {
        this.lastMessage = lastMessage;
        this.timeSent = timeSent;
        this.user = user;
    }

    public Chat() {
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Date getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
