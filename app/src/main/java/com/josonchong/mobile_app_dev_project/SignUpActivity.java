package com.josonchong.mobile_app_dev_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import es.dmoral.toasty.Toasty;

public class SignUpActivity extends AppCompatActivity {
    private EditText zidET;
    private EditText passwordET;
    private EditText confirmPasswordET;
    private FirebaseAuth mAuth;
    private TextView warningTV;
    private ProgressDialog dialog;
    private final String TAG = "SignUpActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mAuth = FirebaseAuth.getInstance();
        zidET = findViewById(R.id.zidET);
        passwordET = findViewById(R.id.passwordET);
        confirmPasswordET = findViewById(R.id.confirmPasswordET);
        warningTV = findViewById(R.id.warningTV);
        dialog = new ProgressDialog(this, android.app.AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);

    }

    //check fields and sign up a firebase user using zmail
    public void signUp(View view) {
        final String zid = zidET.getText().toString();
        final String password = passwordET.getText().toString();
        final String confirmPassword = confirmPasswordET.getText().toString();
        if (checkZidFormat(zid)) {
            if (checkPasswordFormat(password)) {
                if (checkPasswordsMatched(password, confirmPassword)) {
                    dialog.show();
                    mAuth.createUserWithEmailAndPassword(Helper.zidToZmail(zid), password)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "createUserWithEmail:success");
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        user.sendEmailVerification();
                                        dismissDialog();
                                        Toasty.info(SignUpActivity.this, "Your account has been created. A verification email has been sent to your zmail.",
                                                Toast.LENGTH_LONG).show();
                                        finish();
                                    } else {
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        Toasty.error(SignUpActivity.this, "Failed to create user. Please make sure that the ZMail is not used.",
                                                Toast.LENGTH_LONG).show();
                                        dismissDialog();
                                        finish();
                                    }
                                }
                            });
                } else {
                    warningTV.setText("Passwords not matched.");
                }
            } else {
                warningTV.setText("Password should be between 6 - 15 digits");
            }
        } else {
            warningTV.setText("Incorrect ZID format");
        }
    }

    //check if zid is in correct format
    private boolean checkZidFormat(String zid) {
        if (zid.matches("([0-9]{7})")) {
            Log.d(TAG, zid + " is correct zid format");
            return true;
        } else {
            return false;
        }
    }

    //check if password is in correct format
    private boolean checkPasswordFormat(String password) {
        if (password.length() > 5) {
            Log.d(TAG, "password has more then 5 digits");
            return true;
        } else {
            return false;
        }
    }

    //check if 2 passwords match
    private boolean checkPasswordsMatched(String password, String confirmPassword) {
        if (password.equals(confirmPassword)) {
            return true;
        } else {
            return false;
        }
    }

    void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
