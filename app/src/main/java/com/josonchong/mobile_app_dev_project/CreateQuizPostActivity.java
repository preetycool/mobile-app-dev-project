package com.josonchong.mobile_app_dev_project;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

import es.dmoral.toasty.Toasty;

public class CreateQuizPostActivity extends YouTubeBaseActivity {
    private EditText titleET;
    private EditText contentET;
    private EditText topicET;
    private ImageView crossIV;
    private final String TAG = "CreateQuizPostActivity";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz_post);
        titleET = findViewById(R.id.titleET);
        contentET = findViewById(R.id.contentET);
        topicET = findViewById(R.id.topicET);
        crossIV = findViewById(R.id.crossIV);
        dialog = new ProgressDialog(this, android.app.AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leave();
            }
        });

    }

    //direct user to create questions activity
    public void submit(View view){
        String title = titleET.getText().toString();
        String content = contentET.getText().toString();
        String topic = topicET.getText().toString();

        if (!title.equals("")&&!content.equals("")&&!title.equals("")){
            title = "[" + topic + "] " + title;
            QuizPost newQuizPost = new QuizPost(CurrentSession.currentUser, Helper.getCurrentDateTime(), Helper.getCurrentDateTime(), title, content, "quiz", topic);
            Intent intent = new Intent(this, CreateQuizActivity.class);
            intent.putExtra("creatingQuiz", newQuizPost);
            startActivity(intent);
            finish();
        }else{
            Toasty.error(CreateQuizPostActivity.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //override on Back Pressed
    @Override
    public void onBackPressed(){
        leave();
    }


    private void leave(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(CreateQuizPostActivity.this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Do you really want to leave this page?");
        alertBuilder.setMessage("Your changes will be discarded.");
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            } });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
