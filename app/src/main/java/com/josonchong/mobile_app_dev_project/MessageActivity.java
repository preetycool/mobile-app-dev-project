package com.josonchong.mobile_app_dev_project;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class MessageActivity extends AppCompatActivity {
    private final String TAG = "MessageActivity";
    private FirebaseFirestore db;
    private ArrayList<Message> messageArrayList = new ArrayList<>();
    private RecyclerView messageRecycleView;
    private EditText textInputET;
    private User messagingWith;
    private TextView messagingWithTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        this.setTitle(null);
        messagingWithTV = findViewById(R.id.messagingWithTV);
        messageRecycleView = findViewById(R.id.messageRecycleView);
        textInputET = findViewById(R.id.textInputET);
        textInputET = findViewById(R.id.textInputET);
        db = FirebaseFirestore.getInstance();
        Intent intent = getIntent();
        messagingWith = (User) intent.getSerializableExtra("messagingWith");

        //set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        messagingWithTV.setText("z" + messagingWith.getZid() + " " + messagingWith.getFirstName()+ " " + messagingWith.getLastName());
        db.collection("messages").document(CurrentSession.getCurrentUser().getZid()).collection(messagingWith.getZid())
                .orderBy("timeSent", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        System.out.println("value.isEmpty() " + value.isEmpty());
                        messageArrayList.clear();
                        if (value.isEmpty()) {

                        } else {
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e);
                                return;
                            }

                            for (QueryDocumentSnapshot doc : value) {
                                if (doc.getString("messageText") != null) {
                                    Message newMessage = doc.toObject(Message.class);
                                    messageArrayList.add(newMessage);

                                }
                            }
                            initRecyclerView();
                        }
                    }
                });
    }

    //uploading the message to Firestore
    public void sendMessage(View view){
        final String messageText = textInputET.getText().toString();
        if(messageText!="") {
            Message newMessage = new Message(CurrentSession.getCurrentUser(), messagingWith, messageText, new Date());
            Map<String, Object> lastMessage = new HashMap<>();
            lastMessage.put("lastMessage", newMessage.getMessageText());
            lastMessage.put("timeSent", newMessage.getTimeSent());

            db.collection("messages").document(CurrentSession.getCurrentUser().getZid()).collection(messagingWith.getZid())
                    .add(newMessage);
            db.collection("messages").document(CurrentSession.getCurrentUser().getZid()).collection(messagingWith.getZid())
                    .document("lastMessage").set(lastMessage);



            db.collection("messages").document(CurrentSession.getCurrentUser().getZid())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists()) {
                                    List<String> conversations = (List<String>) document.get("conversations");
                                    if(!conversations.contains(messagingWith.getZid())){
                                        System.out.println("!conversations.contains(messagingWith.getZid())");
                                        Map<String, Object> chatMap = new HashMap<>();
                                        conversations.add(messagingWith.getZid());
                                        chatMap.put("conversations", conversations);
                                        db.collection("messages").document(CurrentSession.getCurrentUser().getZid())
                                                .set(chatMap);
                                    }


                                } else {
                                    List<String> conversations = new ArrayList<>();
                                    Map<String, Object> chatMap = new HashMap<>();
                                    conversations.add(messagingWith.getZid());
                                    chatMap.put("conversations", conversations);
                                    db.collection("messages").document(CurrentSession.getCurrentUser().getZid())
                                            .set(chatMap);
                                }
                            } else {
                                List<String> conversations = new ArrayList<>();
                                Map<String, Object> chatMap = new HashMap<>();
                                conversations.add(messagingWith.getZid());
                                chatMap.put("conversations", conversations);
                                db.collection("messages").document(CurrentSession.getCurrentUser().getZid())
                                        .set(chatMap);
                            }
                        }
                    });

            db.collection("messages").document(messagingWith.getZid()).collection(CurrentSession.getCurrentUser().getZid())
                    .add(newMessage);
            db.collection("messages").document(messagingWith.getZid()).collection(CurrentSession.getCurrentUser().getZid())
                    .document("lastMessage").set(lastMessage);

            db.collection("messages").document(messagingWith.getZid())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists()) {
                                    List<String> conversations = (List<String>) document.get("conversations");
                                    if(!conversations.contains(CurrentSession.getCurrentUser().getZid())){
                                        conversations.add(CurrentSession.getCurrentUser().getZid());
                                        Map<String, Object> chatMap = new HashMap<>();
                                        chatMap.put("conversations", conversations);
                                        db.collection("messages").document(messagingWith.getZid())
                                                .set(chatMap);
                                                  }
                                } else {
                                    ArrayList<String> conversations = new ArrayList<>();
                                    conversations.add(CurrentSession.getCurrentUser().getZid());
                                    Map<String, Object> chatMap = new HashMap<>();
                                    chatMap.put("conversations", conversations);
                                    db.collection("messages").document(messagingWith.getZid())
                                            .set(chatMap);
                                }
                            } else {
                                ArrayList<String> conversations = new ArrayList<>();
                                conversations.add(CurrentSession.getCurrentUser().getZid());
                                Map<String, Object> chatMap = new HashMap<>();
                                chatMap.put("conversations", conversations);
                                db.collection("messages").document(messagingWith.getZid())
                                        .set(chatMap);
                            }
                        }
                    });

        }
        textInputET.setText("");
    }

    //
    private void initRecyclerView(){
        MessageRecycleViewAdapter adapter = new MessageRecycleViewAdapter(this, messageArrayList);
        messageRecycleView.setAdapter(adapter);
        messageRecycleView.setLayoutManager(new LinearLayoutManager(this));
        messageRecycleView.scrollToPosition(messageArrayList.size() - 1);
    }
}
