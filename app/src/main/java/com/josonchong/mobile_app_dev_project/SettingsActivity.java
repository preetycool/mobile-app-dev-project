package com.josonchong.mobile_app_dev_project;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class SettingsActivity extends AppCompatActivity {
    private Switch switch1;
    private Switch switch2;
    private Switch switch3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        switch1 = findViewById(R.id.switch1);
        switch2 = findViewById(R.id.switch2);
        switch3 = findViewById(R.id.switch3);
        switch2.setChecked(CurrentSession.getSetting(this, "newPostNotifications"));
        switch3.setChecked(CurrentSession.getSetting(this, "newCommentNotifications"));

        if(!switch2.isChecked()&&!switch3.isChecked()){
            switch1.setChecked(false);
        }

        //if notification is off, other switches are disabled
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    switch2.setChecked(false);
                    switch2.setEnabled(false);
                    switch3.setChecked(false);
                    switch3.setEnabled(false);
                }else{
                    switch2.setChecked(true);
                    switch2.setEnabled(true);
                    switch3.setChecked(true);
                    switch3.setEnabled(true);
                }
            }
        });

        //set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //save button is clicked, settings are saved
    public void saveOnClick(View view){
        CurrentSession.setSetting(this, "newPostNotifications", switch2.isChecked());
        CurrentSession.setSetting(this, "newCommentNotifications", switch3.isChecked());
        Toasty.success(this, "Your settings have been saved, re-start the application to apply.", Toast.LENGTH_LONG, true).show();
    }


}
