package com.josonchong.mobile_app_dev_project;

import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Comment implements Serializable {
    private String authorID;
    private String comment;
    private String datePosted;
    private String documentID;
    private List<String> upVotedBy = new ArrayList<>();
    private List<String> downVotedBy = new ArrayList<>();

    Comment() {
    }

    public Comment(String authorID, String comment, String datePosted) {
        this.authorID = authorID;
        this.comment = comment;
        this.datePosted = datePosted;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(String datePosted) {
        this.datePosted = datePosted;
    }

    public List<String> getUpVotedBy() {
        return upVotedBy;
    }

    public void setUpVotedBy(List<String> upVotedBy) {
        this.upVotedBy = upVotedBy;
    }

    public List<String> getDownVotedBy() {
        return downVotedBy;
    }

    public void setDownVotedBy(List<String> downVotedBy) {
        this.downVotedBy = downVotedBy;
    }

    public void updateUpVoteList(String postDocumentID){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> data = new HashMap<>();
        data.put("upVotedBy", upVotedBy);
        db.collection("post").document(postDocumentID).collection("comments").document(documentID).update(data);
    }
    public void updateDownVoteList(String postDocumentID){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> data = new HashMap<>();
        data.put("downVotedBy", downVotedBy);
        db.collection("post").document(postDocumentID).collection("comments").document(documentID).update(data);
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public String getAuthorID() {
        return authorID;
    }

    public void setAuthorID(String authorID) {
        this.authorID = authorID;
    }
}
