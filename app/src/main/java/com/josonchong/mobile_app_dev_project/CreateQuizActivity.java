package com.josonchong.mobile_app_dev_project;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class CreateQuizActivity extends YouTubeBaseActivity {
    private TextInputLayout textInputLayout6;
    private EditText questionET;
    private EditText option1ET;
    private EditText option2ET;
    private EditText option3ET;
    private EditText option4ET;
    private EditText option5ET;
    private EditText explanationsET;
    private CheckBox answer1CB;
    private CheckBox answer2CB;
    private CheckBox answer3CB;
    private CheckBox answer4CB;
    private CheckBox answer5CB;
    private ArrayList<EditText> allEditTexts = new ArrayList<>();
    private ArrayList<CheckBox> allCheckBoxes = new ArrayList<>();
    private ImageView crossIV;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();;
    private final String TAG = "CreateDisPostActivity";
    private ProgressDialog dialog;
    private QuizPost creatingQuiz;
    private ArrayList<QuizQuestion> quizQuestionArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);
        textInputLayout6 = findViewById(R.id.textInputLayout6);
        explanationsET = findViewById(R.id.explanationsET);
        crossIV = findViewById(R.id.crossIV);
        questionET = findViewById(R.id.questionET);
        option1ET = findViewById(R.id.option1ET);
        option2ET = findViewById(R.id.option2ET);
        option3ET = findViewById(R.id.option3ET);
        option4ET = findViewById(R.id.option4ET);
        option5ET = findViewById(R.id.option5ET);

        answer1CB = findViewById(R.id.answer1CB);
        answer2CB = findViewById(R.id.answer2CB);
        answer3CB = findViewById(R.id.answer3CB);
        answer4CB = findViewById(R.id.answer4CB);
        answer5CB = findViewById(R.id.answer5CB);

        Intent intent = getIntent();
        creatingQuiz = (QuizPost) intent.getSerializableExtra("creatingQuiz");

        allEditTexts.addAll(Arrays.asList(new EditText[]{questionET, option1ET, option2ET, option3ET, option4ET, option5ET}));
        allCheckBoxes.addAll(Arrays.asList(new CheckBox[]{answer1CB, answer2CB, answer3CB, answer4CB, answer5CB}));

        reset();

        dialog = new ProgressDialog(this, android.app.AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leave();
            }
        });
    }

    //upload post to Firestore
    public void submit(View view){
        addQuestion();
        if (quizQuestionArrayList.size()>0){
                creatingQuiz.setQuizQuestions(quizQuestionArrayList);
                ObjectMapper oMapper = new ObjectMapper();
                Map<String, Object> postMap = oMapper.convertValue(creatingQuiz, Map.class);
                showDialog();
                db.collection("posts")
                        .add(postMap)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added" +
                                        "d with ID: " + documentReference.getId());
                                Helper.addDataToFirestoreAsyn("posts", documentReference.getId(), "documentID", documentReference.getId());
                                Toasty.success(CreateQuizActivity.this, "Your post has been created.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toasty.error(CreateQuizActivity.this, "Unexpected error occurred.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        });
        }else{
            Toasty.error(CreateQuizActivity.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //set on click listener for the add question button
    public void addQuestionOnclick(View view){
        if (addQuestion()){
            reset();
        }
    }

    //add a new question
    private boolean addQuestion(){
        ArrayList<CheckBox> cbList = new ArrayList<>();
        cbList.add(answer1CB);
        cbList.add(answer2CB);
        if(!option3ET.getText().toString().equals("")){
            cbList.add(answer3CB);
        }
        if(!option4ET.getText().toString().equals("")){
            cbList.add(answer4CB);
        }
        if(!option5ET.getText().toString().equals("")){
            cbList.add(answer5CB);
        }

        if(checkCBs(cbList)){
            if(!questionET.getText().toString().equals("")&&!option1ET.getText().toString().equals("")&& !option2ET.getText().equals("")){
                ArrayList<Option> options = new ArrayList<>();
                options.add(new Option(option1ET.getText().toString(), answer1CB.isChecked()));
                options.add(new Option(option2ET.getText().toString(), answer2CB.isChecked()));
                if(!option3ET.getText().toString().equals("")){
                    options.add(new Option(option3ET.getText().toString(), answer3CB.isChecked()));
                }
                if(!option4ET.getText().toString().equals("")){
                    options.add(new Option(option4ET.getText().toString(), answer4CB.isChecked()));
                }
                if(!option5ET.getText().toString().equals("")){
                    options.add(new Option(option5ET.getText().toString(), answer5CB.isChecked()));
                }
                quizQuestionArrayList.add(new QuizQuestion(questionET.getText().toString(), options, explanationsET.getText().toString()));
                return true;
            }else{
                Toasty.error(this, "Question, Option 1 and option 2 cannot be empty", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toasty.error(this, "You must at least choose one answer", Toast.LENGTH_SHORT).show();
        }
        return false;
    }



    private void reset(){
        for(int i = 0; i < allEditTexts.size(); i ++){
            allEditTexts.get(i).setText("");
        }
        for(int i = 0; i < allCheckBoxes.size(); i ++){
            allCheckBoxes.get(i).setChecked(false);
        }
        textInputLayout6.setHint("Question " + (quizQuestionArrayList.size()+1) + ":");
        explanationsET.setText("");
    }

    //check at least one checkbox is checked
    private boolean checkCBs(ArrayList<CheckBox> checkBoxes){
        for(int i = 0; i < checkBoxes.size(); i ++){
            if(checkBoxes.get(i).isChecked()){
                return true;
            }
        }
        return false;
    }

    //show dialog if it's not showing
    private void showDialog() {
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    //dismiss dialog if it is showing
    private void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    //override onBackPressed
    @Override
    public void onBackPressed(){
        leave();
    }

    //Confirm if user wants to leave this page
    private void leave(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(CreateQuizActivity.this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Do you really want to leave this page?");
        alertBuilder.setMessage("Your changes will be discarded.");
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            } });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
