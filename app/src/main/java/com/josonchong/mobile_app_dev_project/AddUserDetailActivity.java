package com.josonchong.mobile_app_dev_project;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

import static android.view.View.GONE;

public class AddUserDetailActivity extends AppCompatActivity {
    private String zid;
    private User editingUser;
    private TextView zidTV;
    private TextView textView5;
    private TextView textView6;
    private EditText firstNameET;
    private EditText lastNameET;
    private Spinner majorSpinner;
    private EditText yearET;
    private ProgressDialog dialog;
    private Spinner genderSpinner;
    private EditText iconLinkET;
    private CircleImageView imageView;
    private final String TAG = "AddUserDetailActivity";
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_detail);
        db = FirebaseFirestore.getInstance();
        dialog = new ProgressDialog(this, AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        zidTV = findViewById(R.id. zidTV);
        textView5 = findViewById(R.id. textView5);
        textView6 = findViewById(R.id. textView6);
        firstNameET = findViewById(R.id. firstNameET);
        lastNameET = findViewById(R.id. lastNameET);
        majorSpinner = findViewById(R.id.majorSpinner);
        yearET = findViewById(R.id.yearET);
        genderSpinner = findViewById(R.id.genderSpinner);
        iconLinkET = findViewById(R.id.iconLinkET);
        imageView = (CircleImageView) findViewById(R.id.imageView);

        //set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();

        if(intent.getStringExtra("zid")!=null){
            zid = intent.getStringExtra("zid");
            zidTV.setText("z" + zid);
        }else if(intent.getSerializableExtra("editingUser")!=null){
            editingUser = (User) intent.getSerializableExtra("editingUser");
            zid = editingUser.getZid();
            textView5.setVisibility(GONE);
            textView6.setVisibility(GONE);
            zidTV.setVisibility(GONE);
            firstNameET.setText(editingUser.getFirstName());
            lastNameET.setText(editingUser.getLastName());
            yearET.setText(editingUser.getYear());
            iconLinkET.setText(editingUser.getIconUrl());

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.major_array, android.R.layout.simple_spinner_item);
            majorSpinner.setSelection(adapter.getPosition(editingUser.getMajor()));
            ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.gender_spinner_entries, android.R.layout.simple_spinner_item);
            genderSpinner.setSelection(adapter2.getPosition(editingUser.getGender()));
            if(editingUser.getIconUrl()!=null){
                previewImage(editingUser.getIconUrl());
            }
        }
    }

    //upload user details to firestore
    public void submit(View view){
        final String firstName = firstNameET.getText().toString();
        final String lastName = lastNameET.getText().toString();
        final String year = yearET.getText().toString();
        if (!firstName.equals("")&&!lastName.equals("")&&!year.equals("")) {
            Map<String, Object> user = new HashMap<>();
            user.put("firstName", firstName);
            user.put("lastName", lastName);
            user.put("zid", zid);
            user.put("major", majorSpinner.getSelectedItem().toString());
            System.out.println("majorSpinner.getSelectedItem().toString(): " + majorSpinner.getSelectedItem().toString());
            user.put("year", year);
            user.put("gender", genderSpinner.getSelectedItem().toString());
            if(!iconLinkET.getText().toString().equals("")){
                String link = iconLinkET.getText().toString();
                if(!link.startsWith("http")){
                    link = "http://" + link;
                }
                user.put("iconUrl", link);
            }
            final Map<String, Object> newUser = user;
            dialog.show();
            db.collection("users").document(zid)
                    .set(newUser)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toasty.success(AddUserDetailActivity.this, "User details added, you will be directed to the landing page", Toast.LENGTH_SHORT).show();
                            ObjectMapper objectMapper = new ObjectMapper();
                            CurrentSession.currentUser = objectMapper.convertValue(newUser, User.class);
                            dismissDialog();
                            finish();
                            startActivity(new Intent(AddUserDetailActivity.this, LandingActivity.class));
                        }
                    });
        }else{
            Toasty.error(this, "Fields cannot be empty", Toast.LENGTH_SHORT, true).show();
        }
    }

    //preview icon
    public void preview(View view){
        String link = iconLinkET.getText().toString();
        if(!link.equals("")){
            if(!link.startsWith("http")){
                link = "http://" + link;
                iconLinkET.setText(link);
            }
            previewImage(link);
        }else{
            Toasty.error(this, "Please make sure you have filled in the icon link field").show();
        }

    }

    //load image to the image view with link
    private void previewImage(String link){
            try{
                Picasso.get().load(link).into(imageView);
            }catch (Exception ex){
                Toasty.error(this, "Failed to load image, please make sure you have entered the correct link").show();
            }
    }

    //dismiss dialog if it is showing
    void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
