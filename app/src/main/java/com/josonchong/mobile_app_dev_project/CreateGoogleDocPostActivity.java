package com.josonchong.mobile_app_dev_project;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

import es.dmoral.toasty.Toasty;

public class CreateGoogleDocPostActivity extends YouTubeBaseActivity {
    private EditText titleET;
    private EditText contentET;
    private EditText documentLinkET;
    private EditText documentNameET;
    private ImageView googleDocIV;
    private ImageView crossIV;
    private CheckBox editableCB;
    private CheckBox hideCheckBox;
    private ConstraintLayout constraintLayout4;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();;
    private final String TAG = "CreateGDocPostActivity";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_googledoc_post);
        titleET = findViewById(R.id.titleET);
        contentET = findViewById(R.id.contentET);
        documentLinkET = findViewById(R.id.documentLinkET);
        documentNameET = findViewById(R.id.documentNameET);
        googleDocIV = findViewById(R.id.googleDocIV);
        crossIV = findViewById(R.id.crossIV);
        editableCB = findViewById(R.id.editableCB);
        hideCheckBox = findViewById(R.id.hideCheckBox);
        constraintLayout4 = findViewById(R.id.constraintLayout4);

        //set on checked listener for the hide instructions checkbox
        hideCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //hide instructions if it's showing
                if(hideCheckBox.isChecked()){
                    constraintLayout4.setVisibility(View.GONE);
                }else{
                    constraintLayout4.setVisibility(View.VISIBLE);
                }
            }
        });

        dialog = new ProgressDialog(this, android.app.AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        googleDocIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                //intent.setPackage("com.google.android.apps.docs.editors.docs");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("market://details?id=com.google.android.apps.docs.editors.docs"));
                try {
                    startActivity(intent);
                }catch(Exception ex){
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(CreateGoogleDocPostActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Oops, seems like you don't have Google Docs installed in your device");
                    alertBuilder.setMessage("Do you want to open Google doc with your browser?.");
                    alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent2 = new Intent(Intent.ACTION_VIEW);
                            intent2.setData(Uri.parse("https://docs.google.com"));
                            startActivity(intent2);
                        }
                    });
                }
            }
        });

        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leave();
            }
        });
    }

    //upload post to Firestore
    public void submit(View view){
        final String title = titleET.getText().toString();
        final String content = contentET.getText().toString();
        final String documentName = documentNameET.getText().toString();
        final String documentLink = documentLinkET.getText().toString();
        final boolean editable = editableCB.isChecked();

        if (!title.equals("")&&!content.equals("")&&!documentName.equals("")&&!documentLink.equals("")){
            if (documentLink.contains("doc")){
                GoogleDocPost newGoogleDocPost = new GoogleDocPost(CurrentSession.currentUser, Helper.getCurrentDateTime(), Helper.getCurrentDateTime(), title, content, documentName, documentLink, editable);
                ObjectMapper oMapper = new ObjectMapper();
                Map<String, Object> googleDocPostMap = oMapper.convertValue(newGoogleDocPost, Map.class);
                dialog.show();
                db.collection("posts")
                        .add(googleDocPostMap)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                Helper.addDataToFirestoreAsyn("posts", documentReference.getId(), "documentID", documentReference.getId());
                                Toasty.success(CreateGoogleDocPostActivity.this, "Your post has been created.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                 Toasty.error(CreateGoogleDocPostActivity.this, "Unexpected error occurred.", Toast.LENGTH_SHORT).show();
                                finish();
                                dismissDialog();
                            }
                        });
            }else{
                 Toasty.error(CreateGoogleDocPostActivity.this, "Incorrect link format.", Toast.LENGTH_SHORT).show();
            }
        }else{
             Toasty.error(CreateGoogleDocPostActivity.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //dismiss dialog if it is showing
    private void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    //override onBackPressed
    @Override
    public void onBackPressed(){
        leave();
    }

    //Confirm if user wants to leave this page
    private void leave(){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(CreateGoogleDocPostActivity.this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Do you really want to leave this page?");
        alertBuilder.setMessage("Your changes will be discarded.");
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            } });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }
}
