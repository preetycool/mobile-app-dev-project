package com.josonchong.mobile_app_dev_project;

public class GoogleDocPost extends Post{
    private String url;
    private boolean editable;
    private String documentName;

    GoogleDocPost(){
    }

    GoogleDocPost(User author, String postDate, String lastEdit, String title, String content, String documentName, String url, boolean editable){
        super(author, postDate, lastEdit, title, content, "googleDoc");
        this.documentName = documentName;
        this.url = url;
        this.editable = editable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }
}
