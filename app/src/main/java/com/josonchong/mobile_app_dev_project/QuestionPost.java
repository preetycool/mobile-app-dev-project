package com.josonchong.mobile_app_dev_project;

import java.io.Serializable;

public class QuestionPost extends Post implements Serializable{
    Comment solution;

    public QuestionPost(){

    }

    public QuestionPost(User author, String postDate, String lastEdit, String title, String content) {
        super(author, postDate, lastEdit, title, content, "question");
    }

    public QuestionPost(User author, String postDate, String lastEdit, String title, String content, String topic, Comment solution) {
        super(author, postDate, lastEdit, title, content, "question");
        this.solution = solution;
    }

    public Comment getSolution() {
        return solution;
    }

    public void setSolution(Comment solution) {
        this.solution = solution;
    }
}
