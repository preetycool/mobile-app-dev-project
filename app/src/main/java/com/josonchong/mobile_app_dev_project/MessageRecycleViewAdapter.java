package com.josonchong.mobile_app_dev_project;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

//Reference: https://blog.sendbird.com/android-chat-tutorial-building-a-messaging-ui
public class MessageRecycleViewAdapter extends RecyclerView.Adapter {

    private ArrayList<Message> messageArrayList = new ArrayList<>();
    private Context mContext;
    private final String TAG = "MessageRecycleViewAd";
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM HH:mm");

    public MessageRecycleViewAdapter(Context context, ArrayList<Message> messageArrayList) {
        this.messageArrayList = messageArrayList;
        mContext = context;
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+11"));
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View myView;
        //get item type, 0 = message sent; 1 = message received
        if (i == 0) {
            myView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.layout_message_sent, viewGroup, false);
            return new SentMessageHolder(myView);
        } else if (i == 1) {
            myView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.layout_message_received, viewGroup, false);
            return new ReceivedMessageHolder(myView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Message message = messageArrayList.get(position);
        if(holder.getItemViewType()==0){
            ((SentMessageHolder) holder).bindMessage(message);
        }else if (holder.getItemViewType()==1){
            ((ReceivedMessageHolder) holder).bindMessage(message);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Message message =  messageArrayList.get(position);
        if (message.getSender().getZid().equals(CurrentSession.getCurrentUser().getZid())) {
            //0 = message sent
            return 0;
        } else {
            //1 = message received
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageTextTV;
        TextView timeSentTV;
        TextView senderNameTV;
        CircleImageView senderIconCIV;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageTextTV = itemView.findViewById(R.id.messageTextTV);
            timeSentTV = itemView.findViewById(R.id.timeSentTV);
            senderNameTV = itemView.findViewById(R.id.senderNameTV);
            senderIconCIV = (CircleImageView) itemView.findViewById(R.id.senderIconCIV);
        }

        void bindMessage(Message message) {
            messageTextTV.setText(message.getMessageText());
            timeSentTV.setText(sdf.format(message.getTimeSent()));
            senderNameTV.setText(message.getSender().getFirstName());
            try {
                Picasso.get().load(message.getSender().getIconUrl()).into(senderIconCIV);
            }catch (Exception ex){
                Picasso.get().load(R.mipmap.maleicon_round).into(senderIconCIV);
            }
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageTextTV;
        TextView timeSentTV;

        SentMessageHolder(View itemView) {
            super(itemView);
            messageTextTV = itemView.findViewById(R.id.messageTextTV);
            timeSentTV = itemView.findViewById(R.id.timeSentTV);
        }

        void bindMessage(Message message) {
            messageTextTV.setText(message.getMessageText());
            timeSentTV.setText(sdf.format(message.getTimeSent()));
        }
    }
}
