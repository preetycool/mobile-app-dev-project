package com.josonchong.mobile_app_dev_project;

import java.io.Serializable;

public class User implements Serializable{
    private String zid;
    private String firstName;
    private String lastName;
    private String major;
    private String year;
    private String iconUrl;
    private String gender = "M";

    User(){
    }

    public User(String zid, String firstName, String iconUrl) {
        this.zid = zid;
        this.firstName = firstName;
        this.iconUrl = iconUrl;
    }

    public String getZid() {
        return zid;
    }

    public void setZid(String zid) {
        this.zid = zid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
