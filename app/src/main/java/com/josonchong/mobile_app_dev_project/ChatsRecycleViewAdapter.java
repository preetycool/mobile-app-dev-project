package com.josonchong.mobile_app_dev_project;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

//Following tutorial: https://www.youtube.com/watch?v=Vyqz_-sJGFk
public class ChatsRecycleViewAdapter extends RecyclerView.Adapter<ChatsRecycleViewAdapter.ViewHolder> {

    private ArrayList<Chat> chatArrayList = new ArrayList<>();
    private Context mContext;
    private final String TAG = "ChatsRecycleViewAd";
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM HH:mm");


    public ChatsRecycleViewAdapter(Context context, ArrayList<Chat> chatArrayList) {
        this.chatArrayList = chatArrayList;
        mContext = context;
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+11"));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_chats, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final ViewHolder v = viewHolder;
        final Chat bindChat = chatArrayList.get(i);

        if(bindChat!=null){
            FirebaseFirestore.getInstance().collection("users").document(bindChat.getUser().getZid())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists()) {
                                    v.userNameTV.setText(document.getString("firstName"));
                                    v.messagePreviewTV.setText(bindChat.getLastMessage());
                                    v.timeSentTV.setText(sdf.format(bindChat.getTimeSent()));
                                    try {
                                        if(document.getString("iconUrl")!=null) {
                                            if(!document.getString("iconUrl").equals("")) {
                                                Picasso.get().load(document.getString("iconUrl")).into(v.image);
                                            }else{
                                                Picasso.get().load(R.mipmap.maleicon_round).into(v.image);
                                            }
                                        }else{
                                            Picasso.get().load(R.mipmap.maleicon_round).into(v.image);
                                        }
                                    }catch (Exception ex){
                                        Picasso.get().load(R.mipmap.maleicon_round).into(v.image);
                                    }
                                } else {

                                }
                            } else {

                            }
                        }
                    });
        }

        //get user and start chat after the recycler view is clicked
        viewHolder.chats_parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, " viewHolder.chats_parent_layout on click");
                Helper.startChatWithZid(mContext, bindChat.getUser().getZid());
            }
        });
    }

    @Override
    public int getItemCount() {
        return chatArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        TextView userNameTV;
        TextView messagePreviewTV;
        TextView timeSentTV;
        LinearLayout chats_parent_layout;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (CircleImageView) itemView.findViewById(R.id.imageView);
            userNameTV = itemView.findViewById(R.id.userNameTV);
            messagePreviewTV = itemView.findViewById(R.id.messagePreviewTV);
            timeSentTV = itemView.findViewById(R.id.timeSentTV);
            chats_parent_layout = itemView.findViewById(R.id.chats_parent_layout);
        }
    }
}