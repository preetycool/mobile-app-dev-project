package com.josonchong.mobile_app_dev_project;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity {
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private ProgressDialog dialog;
    private EditText zidET;
    private EditText passwordET;
    private TextView warningTV;
    private final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        dialog = new ProgressDialog(this, AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        zidET = findViewById(R.id.zidET);
        passwordET = findViewById(R.id.passwordET);
        warningTV = findViewById(R.id.warningTV);
        db = FirebaseFirestore.getInstance();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        //Reference: https://stackoverflow.com/questions/11178041/android-youtube-share-a-youtube-video-to-my-app
        //check if user has already logged in, if yes, bring next activity
        if (currentUser != null) {
            Intent intent = getIntent();
            String action = intent.getAction();
            String type = intent.getType();
            Log.d(TAG, "type: " + type);
            Log.d(TAG, "action: " + action);

            //check if there is a youtube intent, if yes, bring user to create youtube page
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if (type.equals("text/plain")) {
                    Bundle bundle = getIntent().getExtras();
                    final String text = bundle.getString(Intent.EXTRA_TEXT);
                    if (text.contains("youtu") || text.contains("watch?v=")) {
                        loginWithActionYT(currentUser, intent);
                    }
                }
            } else {
                loginWithZid(Helper.ZmailToZid(currentUser.getEmail()));
            }
        }
    }

    //login button clicked, verify firebase login
    public void loginBtnClicked(View view) {
        warningTV.setText("");
        final String zid = Helper.getZidWithoutZ(zidET.getText().toString());
        final String zmail = Helper.zidToZmail(zid);
        Log.d(TAG, "zmail: " + zmail);
        if (!zid.equals("") && !passwordET.getText().toString().equals("")) {
            showDialog();
            mAuth.signInWithEmailAndPassword(zmail, passwordET.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Firebase Login success.");
                                //Toast.makeText(LoginActivity.this, "signInWithEmail:success", Toast.LENGTH_SHORT ).show();
                                FirebaseUser user = mAuth.getCurrentUser();
                                if (user.isEmailVerified()) {
                                    loginWithZid(zid);
                                } else {
                                    Toasty.info(LoginActivity.this, "Please verify your email. If you cannot find the verification email in your normal inbox, it is worth checking in your spam or junk mail section.").show();
                                    user.sendEmailVerification();
                                }
                                dismissDialog();
                            } else {
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toasty.error(LoginActivity.this, "Authentication failed.").show();
                                dismissDialog();
                            }
                        }
                    });
        } else {
            Toasty.error(LoginActivity.this, "Fields cannot be empty.").show();
        }
    }

    //called when firebase authentication success, find if zid exist in the database. If yes, bring user to landing page; if no, bring user to add user detail activity.
    private void loginWithZid(String studentID) {
        final String zid = studentID;
        DocumentReference docRef = db.collection("users").document(zid);
        showDialog();
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.getString("zid") != null) {
                        User currentUser = task.getResult().toObject(User.class);
                        CurrentSession.currentUser = currentUser;
                        Intent intent = new Intent(LoginActivity.this, LandingActivity.class);
                        startActivity(intent);
                        dismissDialog();
                    } else {
                        Log.d(TAG, "User doesn't exist");
                        Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                        intent.putExtra("zid", zid);
                        startActivity(intent);
                        dismissDialog();
                    }
                } else {
                    Log.d(TAG, "User not found");
                    Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                    intent.putExtra("zid", zid);
                    startActivity(intent);
                    dismissDialog();
                }
            }
        });
    }

    //bring user to signup activity
    public void signupBtnClicked(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    //show dialog if not showing
    private void showDialog() {
        try {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception ex) {

        }
    }

    //dismiss dialog if showing
    private void dismissDialog() {
        try {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception ex) {

        }
    }

    //used when there is a youtube intent, start create youtube post and landing activities
    private void loginWithActionYT(FirebaseUser currentUser, Intent intent) {
        final Intent newIntent = intent;
        final String zid = Helper.ZmailToZid(currentUser.getEmail());
        db = FirebaseFirestore.getInstance();
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("users").document(zid);
        showDialog();
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.getString("zid") != null) {
                        User currentUser = task.getResult().toObject(User.class);
                        CurrentSession.currentUser = currentUser;
                        Intent intent = new Intent(LoginActivity.this, LandingActivity.class);
                        newIntent.setClass(LoginActivity.this, CreateYoutubePostActivity.class);
                        Intent intentArray[] = {intent, newIntent};
                        startActivities(intentArray);
                        dismissDialog();
                    } else {
                        Log.d(TAG, "No such document");
                        Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                        intent.putExtra("zid", zid);
                        startActivity(intent);
                        dismissDialog();
                    }
                } else {
                    Log.d(TAG, "User not found");
                    Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                    intent.putExtra("zid", zid);
                    startActivity(intent);
                    dismissDialog();
                }
            }
        });
    }

    //Outdated version, this is for backup purpose. Do not use this.
    private void loginWithZidBackup(String studentID) {
        final String zid = studentID;
        //Toast.makeText(LoginActivity.this, ("LoginWithZID " + zid), Toast.LENGTH_SHORT).show();
        db = FirebaseFirestore.getInstance();
        showDialog();
        db.collection("users")
                .whereEqualTo("zid", zid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        int count = 0;
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Map<String, Object> user = document.getData();
                                User currentUser = new User(user.get("zid").toString(), user.get("firstName").toString(), user.get("iconUrl").toString());
                                CurrentSession.currentUser = currentUser;
                                Intent intent = new Intent(LoginActivity.this, LandingActivity.class);
                                startActivity(new Intent(intent));
                                count++;
                            }
                            if (count == 0) {
                                Log.w(TAG, "count 0", task.getException());
                                Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                                intent.putExtra("zid", zid);
                                startActivity(new Intent(intent));
                            }
                            dismissDialog();
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                            intent.putExtra("zid", zid);
                            startActivity(new Intent(intent));
                            dismissDialog();
                        }
                    }
                });
    }

    //Outdated version, this is for backup purpose. Do not use this.
    private void loginWithActionYTBackup(FirebaseUser currentUser, Intent intent) {
        final Intent newIntent = intent;
        final String zid = Helper.ZmailToZid(currentUser.getEmail());
        //Toast.makeText(LoginActivity.this, ("LoginWithZID " + zid), Toast.LENGTH_SHORT).show();
        db = FirebaseFirestore.getInstance();
        showDialog();
        db.collection("users")
                .whereEqualTo("zid", zid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        int count = 0;
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Map<String, Object> user = document.getData();
                                User currentUser = new User(user.get("zid").toString(), user.get("firstName").toString(), user.get("iconUrl").toString());
                                CurrentSession.currentUser = currentUser;
                                Intent intent = new Intent(LoginActivity.this, LandingActivity.class);
                                //startActivity(new Intent(intent));
                                count++;

                                newIntent.setClass(LoginActivity.this, CreateYoutubePostActivity.class);

                                Intent intentArray[] = {intent, newIntent};
                                startActivities(intentArray);
                                //startActivity(newIntent);
                            }
                            if (count == 0) {
                                Log.w(TAG, "count 0", task.getException());
                                Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                                intent.putExtra("zid", zid);
                                startActivity(new Intent(intent));
                            }
                            dismissDialog();
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            Intent intent = new Intent(LoginActivity.this, AddUserDetailActivity.class);
                            intent.putExtra("zid", zid);
                            startActivity(new Intent(intent));
                            dismissDialog();
                        }
                    }
                });
    }
}


