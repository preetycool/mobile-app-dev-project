package com.josonchong.mobile_app_dev_project;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentsRecycleViewAdapter extends RecyclerView.Adapter<CommentsRecycleViewAdapter.ViewHolder>{
    private ArrayList<Comment> commentArrayList = new ArrayList<>();
    private Context mContext;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final String TAG = "CommentsRecycleViewAd";
    private QuestionPost viewingPost;
    private Activity activity;

    public CommentsRecycleViewAdapter(Context context, ArrayList<Comment> commentArrayList) {
        this.commentArrayList = commentArrayList;
        mContext = context;
    }
    public CommentsRecycleViewAdapter(Context context, ArrayList<Comment> commentArrayList, QuestionPost viewingPost, Activity activity) {
        this.commentArrayList = commentArrayList;
        mContext = context;
        this.viewingPost = viewingPost;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_comments, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Comment bindComment = commentArrayList.get(i);
        if(bindComment.getDocumentID()!=null) {
            DocumentReference docRef = db.collection("users").document(bindComment.getAuthorID());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document != null && document.getString("zid") != null) {
                            User user = task.getResult().toObject(User.class);
                            if (user != null) {
                                viewHolder.datePosted.setText(bindComment.getDatePosted());
                                viewHolder.expand_text_view.setText(bindComment.getComment());
                                if (user.getIconUrl() != null) {
                                    if(!user.getIconUrl().equals("")) {
                                        try {
                                            Picasso.get().load(user.getIconUrl()).into(viewHolder.image);
                                        } catch (Exception ex) {
                                            loadMorFIcon(user, viewHolder.image);
                                        }
                                    }else{
                                        loadMorFIcon(user, viewHolder.image);
                                    }
                                } else {
                                    loadMorFIcon(user, viewHolder.image);
                                }
                                viewHolder.author.setText(user.getFirstName());
                            }
                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "User not found");
                    }
                }

            });

            //user's name on click, display user_options_menu
            viewHolder.author.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ViewUserActivity.class);
                    intent.putExtra("zid", bindComment.getAuthorID());
                    mContext.startActivity(intent);
                }
            });

            //user's icon on click, display user_options_menu
            viewHolder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ViewUserActivity.class);
                    intent.putExtra("zid", bindComment.getAuthorID());
                    mContext.startActivity(intent);
                }
            });

            //more button on top right corner clicked, display question post_comment_menu for question posts, otherwise display normal_comment_menu
            //questionpost_comment_menu has the option of select as solution
            viewHolder.textViewOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(mContext, viewHolder.textViewOptions);
                    if (viewingPost == null) {
                        popupMenu.inflate(R.menu.normal_comment_menu);
                    } else {
                        popupMenu.inflate(R.menu.questionpost_comment_menu);
                        if (viewingPost.solution == null && viewingPost.getAuthor().getZid().equals(CurrentSession.currentUser.getZid())) {
                            popupMenu.getMenu().getItem(1).setEnabled(true);
                        } else {
                            popupMenu.getMenu().getItem(1).setEnabled(false);
                        }
                    }
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.reportMenu:
                                    report(bindComment);
                                    break;
                                case R.id.selectMenu:
                                    select(bindComment);
                                    break;
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });
        }
    }

    //load icon depends on user's gender
    private void loadMorFIcon(User user, CircleImageView circleImageView){
        if (user.getGender().equals("M")){
            Picasso.get().load(R.mipmap.maleicon_round).into(circleImageView);
        }else{
            Picasso.get().load(R.mipmap.femaleicon_round).into(circleImageView);
        }
    }

    //report menu item clicked, start email intent
    private void report(Comment bindComment){
        Helper.sendEmail(mContext, "Reporting comment " + bindComment.getDocumentID(), "Content: \"" + bindComment.getComment() + "\"\n\n" + "Reason(s) of reporting:\n ");
    }

    //select as solution menu item clicked, select comment as the solution for the post
    private void select(Comment bindComment){
        viewingPost.setSolution(bindComment);
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> postMap = oMapper.convertValue(viewingPost, Map.class);

        db.collection("posts").document(viewingPost.getDocumentID()).update(postMap);
    }

    @Override
    public int getItemCount() {
        return commentArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView author;
        TextView datePosted;
        ExpandableTextView expand_text_view;
        RelativeLayout comments_parent_layout;
        TextView textViewOptions;

        public ViewHolder(View itemView){
            super(itemView);
            image = (CircleImageView) itemView.findViewById(R.id.imageView);
            author = itemView.findViewById(R.id.userEmailTV);
            datePosted = itemView.findViewById(R.id.datePosted);
            expand_text_view = (ExpandableTextView) itemView.findViewById(R.id.expand_text_view);
            comments_parent_layout = itemView.findViewById(R.id.comments_parent_layout);
            textViewOptions = itemView.findViewById(R.id.timeSentTV);
        }

    }
}

