package com.josonchong.mobile_app_dev_project;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class ViewYoutubePost extends YouTubeBaseActivity {
    private YoutubePost viewingPost;
    private static final String API_KEY = "AIzaSyA9vNt6uq4ZSTBBzA6yTRBeeOJr9-ANSbA";
    private ProgressDialog dialog;
    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    private ArrayList<Comment> commentArrayList = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private TextView title;
    private TextView time;
    private ExpandableTextView expandableTextView;
    private TextView author;
    private EditText textInputET;
    private Button likeBtn;
    private Button dislikeBtn;
    private Button reportBtn;
    private String TAG = "ViewYoutubePost";
    private String viewingPostDocumentID;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_youtube_post);
        Intent intent = getIntent();
        viewingPostDocumentID = intent.getStringExtra("viewingPost");
        //viewingPost = (YoutubePost) intent.getSerializableExtra("viewingPost");
        dialog = new ProgressDialog(this, AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        youTubePlayerView = findViewById(R.id.youtubePlayerView);
        title = findViewById(R.id.title);
        time = findViewById(R.id.time);
        expandableTextView = (ExpandableTextView) findViewById(R.id.expand_text_view);
        frameLayout = findViewById(R.id.cons);

        author = findViewById(R.id.userEmailTV);
        textInputET = findViewById(R.id.textInputET);
        likeBtn = findViewById(R.id.likeBtn);
        dislikeBtn = findViewById(R.id.dislikeBtn);
        reportBtn = findViewById(R.id.reportBtn);

        //set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            showDialog();
        }catch (Exception ex){
            Log.w(TAG, ex);
        }
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("posts").document(viewingPostDocumentID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.getString("url") != null) {
                        viewingPost = task.getResult().toObject(YoutubePost.class);
                        try {
                            playVideo(viewingPost.getShortenUrl());
                        }catch (Exception ex){
                            Toasty.error(ViewYoutubePost.this, "Sorry, an unexpected error has occurred. Please try again later.").show();
                            finish();
                        }
                        title.setText(viewingPost.getTitle());
                        time.setText("at " + viewingPost.getPostDate());
                        expandableTextView.setText(viewingPost.getContent());
                        author.setText("Post by: " + viewingPost.getAuthor().getFirstName());
                        author.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(ViewYoutubePost.this, ViewUserActivity.class);
                                intent.putExtra("zid", viewingPost.getAuthor().getZid());
                                startActivity(intent);
                            }
                        });
                        initBtns();
                    } else {
                        Toasty.error(ViewYoutubePost.this, "The post you are looking for was deleted.", Toast.LENGTH_LONG).show();
                        dismissDialog();
                        finish();
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                    Toasty.error(ViewYoutubePost.this, "The post you are looking for is deleted.", Toast.LENGTH_LONG).show();
                    dismissDialog();
                    finish();
                }
            }
        });
    }

    //Snapshot listeners are attached to the activity, will detach if it's paused, so re-attach in onResume
    @Override
    protected void onResume() {
        addPostListener();
        addCommentsListener();
        super.onResume();
    }

    //add snapshot listener to the post
    public void addPostListener() {
        db.collection("posts").document(viewingPostDocumentID)
                .addSnapshotListener(ViewYoutubePost.this, MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (snapshot.getString("title") != null) {
                            viewingPost = snapshot.toObject(YoutubePost.class);
                            title.setText(viewingPost.getTitle());
                            time.setText("at " + viewingPost.getPostDate());
                            expandableTextView.setText(viewingPost.getContent());
                            author.setText("Post by: " + viewingPost.getAuthor().getFirstName());
                            initBtns();

                        }
                    }
                });
    }

    //add snapshot listener to the comments
    public void addCommentsListener() {
        db.collection("posts").document(viewingPostDocumentID).collection("comments")
                .orderBy("datePosted", Query.Direction.DESCENDING)
                .addSnapshotListener(ViewYoutubePost.this, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }
                        commentArrayList.clear();
                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.getString("comment") != null) {
                                Comment newComment = doc.toObject(Comment.class);
                                commentArrayList.add(newComment);
                            }
                        }
                        initRecycleView();
                    }
                });
    }

    //play video in youtubePlayer view with url
    void playVideo(String url) {
        final String videoUrl = url;
        try {
            System.out.println("url: " + url);
            onInitializedListener = new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                    youTubePlayer.cueVideo(videoUrl);
                    dismissDialog();
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                    Toasty.error(ViewYoutubePost.this, "Sorry, unexpected error(s) occurred. Please try again later or contact customer support", Toast.LENGTH_LONG).show();
                    playVideo(videoUrl);
                    dismissDialog();
                }
            };
            try {
                youTubePlayerView.initialize(API_KEY, onInitializedListener);
            } catch (Exception ex) {
                System.out.println(ex);
                Toasty.error(ViewYoutubePost.this, "Sorry, unexpected error(s) occurred. Please try again later or contact customer support", Toast.LENGTH_LONG).show();
                dismissDialog();
            }
        } catch (Exception ex) {
            Toasty.error(ViewYoutubePost.this, "Sorry, unexpected error(s) occurred. Please try again later or contact customer support", Toast.LENGTH_LONG).show();
            dismissDialog();
        }
    }

    /*
    private void loadComments(){
        showDialog();
        db.collection("posts").document(viewingPost.getDocumentID()).collection("comments")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Comment newComment = document.toObject(Comment.class);
                                User user = (User)document.get("Author");
                                newComment.setAuthor(user);
                                System.out.println("newComment.getComment(): " + newComment.getComment());
                                commentArrayList.add(newComment);
                                initRecycleView();
                                dismissDialog();
                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            dismissDialog();
                        }
                    }
                });
    }

*/

    //make comment on the post
    public void commentBtnClicked(View view) {
        final String content = textInputET.getText().toString();
        if (!content.equals("")) {
            textInputET.setText("");
            Comment newComment = new Comment(CurrentSession.currentUser.getZid(), content, Helper.getCurrentDateTime());
            ObjectMapper oMapper = new ObjectMapper();
            Map<String, Object> commentMap = oMapper.convertValue(newComment, Map.class);
            db.collection("posts").document(viewingPost.getDocumentID()).collection("comments")
                    .add(commentMap)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            Helper.addDataToFirestoreAsyn("posts", documentReference.getId(), "documentID", documentReference.getId());
                            Map<String, Object> data = new HashMap<>();
                            data.put("documentID", documentReference.getId());
                            db.collection("posts").document(viewingPost.getDocumentID()).collection("comments").document(documentReference.getId()).update(data);
                            Toasty.success(ViewYoutubePost.this, "Your comment has been created.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toasty.error(ViewYoutubePost.this, "Unexpected error occurred.", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Toasty.error(ViewYoutubePost.this, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }

    //testing purposes only, do not use this
    void TestLoadComments() {
        commentArrayList.add(new Comment("50000", "Testing", Helper.getCurrentDateTime()));
        commentArrayList.add(new Comment("50000", "Testing2", Helper.getCurrentDateTime()));
        initRecycleView();
    }

    //initialization comment recycler view
    private void initRecycleView() {
        RecyclerView recyclerView = findViewById(R.id.recycleView);
        CommentsRecycleViewAdapter adapter = new CommentsRecycleViewAdapter(this, commentArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    //send an email to report the post (bring up email intent)
    public void report(View view) {
        Helper.sendEmail(this, "Reporting post " + viewingPost.getDocumentID(), "Title: \"" + viewingPost.getTitle() + "\"\n" + "Content: \"" + viewingPost.getContent() + "\"\n" + "Video Url: \"" + viewingPost.getUrl() + "\"\n\n" + "Reason(s) of reporting:\n ");
    }

    //like on click, upload the user's zid to the upvote list of the post
    public void likeBtnClicked(View view) {
        if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
            if (viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                viewingPost.getDownVotedBy().remove(CurrentSession.currentUser.getZid());
                viewingPost.updateDownVoteList();
            }
            viewingPost.getUpVotedBy().add(CurrentSession.currentUser.getZid());
        } else {
            viewingPost.getUpVotedBy().remove(CurrentSession.currentUser.getZid());
        }
        viewingPost.updateUpVoteList();
        initBtns();
    }

    //dislike on click, upload the user's zid to the downvote list of the post
    public void dislikeBtnClicked(View view) {
        if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
            if (viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
                viewingPost.getUpVotedBy().remove(CurrentSession.currentUser.getZid());
                viewingPost.updateUpVoteList();
            }
            viewingPost.getDownVotedBy().add(CurrentSession.currentUser.getZid());
        } else {
            viewingPost.getDownVotedBy().remove(CurrentSession.currentUser.getZid());
        }
        viewingPost.updateDownVoteList();
        initBtns();
    }

    //check version of user's phone, set like or dislike button's background/ background hint to dark grey if the user has liked/ disliked the post
    private void initBtns() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {
                likeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.BLACK)));
            } else {
                likeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.GREEN)));
            }

            if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                dislikeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.BLACK)));
            } else {
                dislikeBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            }
            likeBtn.setText("Like (" + viewingPost.getUpVotedBy().size() + ")");
            dislikeBtn.setText("dislike (" + viewingPost.getDownVotedBy().size() + ")");
        } else {
            if (!viewingPost.getUpVotedBy().contains(CurrentSession.currentUser.getZid())) {

                likeBtn.setBackgroundColor(getResources().getColor(R.color.GREY));

            } else {
                likeBtn.setBackgroundColor(getResources().getColor(R.color.DARKGREY));
            }

            if (!viewingPost.getDownVotedBy().contains(CurrentSession.currentUser.getZid())) {
                dislikeBtn.setBackgroundColor(getResources().getColor(R.color.GREY));
            } else {
                dislikeBtn.setBackgroundColor(getResources().getColor(R.color.DARKGREY));
            }

            likeBtn.setText("Like (" + viewingPost.getUpVotedBy().size() + ")");
            dislikeBtn.setText("dislike (" + viewingPost.getDownVotedBy().size() + ")");
        }
    }

    //show dialog if not showing
    private void showDialog() {
        try {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception ex) {

        }
    }

    //dismiss dialog if showing
    private void dismissDialog() {
        try {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception ex) {

        }
    }
}
