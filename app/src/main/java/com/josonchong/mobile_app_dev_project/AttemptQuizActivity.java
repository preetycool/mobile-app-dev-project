package com.josonchong.mobile_app_dev_project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;

public class AttemptQuizActivity extends AppCompatActivity {

    private ImageView o1IV;
    private ImageView o2IV;
    private ImageView o3IV;
    private ImageView o4IV;
    private ImageView o5IV;
    private TextView questionNumberTV;
    private TextView questionTV;
    private TextView o1TV;
    private TextView o2TV;
    private TextView o3TV;
    private TextView o4TV;
    private TextView o5TV;
    private CheckBox o1CB;
    private CheckBox o2CB;
    private CheckBox o3CB;
    private CheckBox o4CB;
    private CheckBox o5CB;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private Button nextBtn;
    private TextView explanationsTV;
    private QuizPost viewingQuiz;
    private int viewingQuestion = 0;
    private ArrayList<ImageView> allOptionImageViews = new ArrayList<>();
    private ArrayList<CheckBox> allOptionCheckBoxes = new ArrayList<>();
    private ArrayList<TextView> allNumberTextViews = new ArrayList<>();
    private ArrayList<CheckBox> all3to5CheckBoxes = new ArrayList<>();
    private ArrayList<TextView> all3to5TextViews = new ArrayList<>();
    private ArrayList<ImageView> all3to5ImageViews = new ArrayList<>();
    private boolean answerChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attempt_quiz);
        questionNumberTV = findViewById(R.id.questionNumberTV);
        questionTV = findViewById(R.id.questionTV);
        nextBtn = findViewById(R.id.nextBtn);
        explanationsTV = findViewById(R.id.explanationsTV);
        o1IV = findViewById(R.id.o1IV);
        o2IV = findViewById(R.id.o2IV);
        o3IV = findViewById(R.id.o3IV);
        o4IV = findViewById(R.id.o4IV);
        o5IV = findViewById(R.id.o5IV);
        o1TV = findViewById(R.id.o1TV);
        o2TV = findViewById(R.id.o2TV);
        o3TV = findViewById(R.id.o3TV);
        o4TV = findViewById(R.id.o4TV);
        o5TV = findViewById(R.id.o5TV);
        o1CB = findViewById(R.id.o1CB);
        o2CB = findViewById(R.id.o2CB);
        o3CB = findViewById(R.id.o3CB);
        o4CB = findViewById(R.id.o4CB);
        o5CB = findViewById(R.id.o5CB);
        tv3 = findViewById(R.id.tv3);
        tv4 = findViewById(R.id.tv4);
        tv5 = findViewById(R.id.tv5);

        //the set onClick listener for the back button in the toolbar
        ImageView crossIV = findViewById(R.id.crossIV);
        crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        explanationsTV.setVisibility(View.GONE);

        //add the views into an arraylist for the convenience of further operations
        allOptionImageViews.addAll(Arrays.asList(new ImageView[]{o1IV, o2IV, o3IV, o4IV, o5IV}));
        allOptionCheckBoxes.addAll(Arrays.asList(new CheckBox[]{o1CB, o2CB, o3CB, o4CB, o5CB}));
        all3to5TextViews.addAll(Arrays.asList(new TextView[]{o3TV, o4TV, o5TV}));
        all3to5CheckBoxes.addAll(Arrays.asList(new CheckBox[]{o3CB, o4CB, o5CB}));
        allNumberTextViews.addAll(Arrays.asList(new TextView[]{tv3, tv4, tv5}));
        all3to5ImageViews.addAll(Arrays.asList(new ImageView[]{o3IV, o4IV, o5IV}));
        Intent intent = getIntent();
        viewingQuiz = (QuizPost) intent.getSerializableExtra("viewingQuiz");
        loadQuestion();
    }

    //set onClick listener for Next button, if it is the last question, exit the activity
    public void nextOnclick(View view){
        if(answerChecked){
            if((viewingQuiz.getQuizQuestions().size()-1)>viewingQuestion){
                viewingQuestion++;
                loadQuestion();
                explanationsTV.setText("");
                explanationsTV.setVisibility(View.GONE);
                nextBtn.setText("Check Answer");
            }else{
                System.out.println("finish");
                finish();
            }
        }else{
            checkAnswer(viewingQuiz.getQuizQuestions().get(viewingQuestion));
            explanationsTV.setVisibility(View.VISIBLE);
            explanationsTV.setText("Explanations: " + viewingQuiz.getQuizQuestions().get(viewingQuestion).getExplanations());
            if(viewingQuiz.getQuizQuestions().size() ==(viewingQuestion+1)){
                nextBtn.setText("Finish");
            }else{
                nextBtn.setText("Next Question");
            }
        }
    }

    //load the current question
    private void loadQuestion(){
        reset();
        for(int i = 0; i < allOptionCheckBoxes.size(); i++){
            allOptionCheckBoxes.get(i).setEnabled(true);
        }
        questionNumberTV.setText("Question " + (viewingQuestion + 1) + "/" + viewingQuiz.getQuizQuestions().size() + ":");
        questionTV.setText(viewingQuiz.getQuizQuestions().get(viewingQuestion).getQuestion() + " (" + viewingQuiz.getQuizQuestions().get(viewingQuestion).getNumberOfAnswers() + " answer(s) should be selected)");
        fillOptions(viewingQuiz.getQuizQuestions().get(viewingQuestion));
        answerChecked = false;
    }

    //make option 3-5 invisible
    private void reset(){
        for(int i = 0; i < allOptionCheckBoxes.size(); i ++){
            allOptionCheckBoxes.get(i).setChecked(false);
            allOptionImageViews.get(i).setVisibility(View.INVISIBLE);
        }
        for (int i = 0; i < all3to5CheckBoxes.size(); i++){
            all3to5CheckBoxes.get(i).setVisibility(View.GONE);
            all3to5TextViews.get(i).setVisibility(View.GONE);
            allNumberTextViews.get(i).setVisibility(View.GONE);
        }
    }

    //load the options into the textViews
    private void fillOptions(QuizQuestion question){
        o1TV.setText(question.getOptions().get(0).getOption());
        o2TV.setText(question.getOptions().get(1).getOption());
        if(question.getOptions().size()>2){
            setOption3Visible(View.VISIBLE);
            o3TV.setText(question.getOptions().get(2).getOption());
        }
        if(question.getOptions().size()>3){
            setOption4Visibility(View.VISIBLE);
            o4TV.setText(question.getOptions().get(3).getOption());
        }
        if(question.getOptions().size()>4){
            setOption5Visibility(View.VISIBLE);
            o5TV.setText(question.getOptions().get(4).getOption());
        }
    }

    //change the visibility of each option
    private void setOption3Visible(int visibility){
        o3TV.setVisibility(visibility);
        o3CB.setVisibility(visibility);
        tv3.setVisibility(visibility);
    }
    private void setOption4Visibility(int visibility){
        o4TV.setVisibility(visibility);
        o4CB.setVisibility(visibility);
        tv4.setVisibility(visibility);
    }
    private void setOption5Visibility(int visibility){
        o5TV.setVisibility(visibility);
        o5CB.setVisibility(visibility);
        tv5.setVisibility(visibility);
    }

    //check answers
    private void checkAnswer(QuizQuestion question){
        for(int i = 0; i < allOptionCheckBoxes.size(); i++){
            allOptionCheckBoxes.get(i).setEnabled(false);
        }
        loadIV(question.getOptions().get(0).isAnswer(), o1CB.isChecked(), o1IV);
        loadIV(question.getOptions().get(1).isAnswer(), o2CB.isChecked(), o2IV);
        if(question.getOptions().size()>2){

            loadIV(question.getOptions().get(2).isAnswer(), o3CB.isChecked(), o3IV);
        }
        if(question.getOptions().size()>3){
            loadIV(question.getOptions().get(3).isAnswer(), o4CB.isChecked(), o4IV);
        }
        if(question.getOptions().size()>4){
            loadIV(question.getOptions().get(4).isAnswer(), o5CB.isChecked(), o5IV);
        }
        answerChecked = true;
    }

    //load tick or cross into the image view
    private void loadIV(boolean answer, boolean isChecked, ImageView imageView){

        if(answer){
            imageView.setVisibility(View.VISIBLE);
            if(isChecked){
                Picasso.get().load(R.mipmap.tickicon).into(imageView);
            }else{
                Picasso.get().load(R.mipmap.red_cross_icon).into(imageView);
            }
        }else{
            imageView.setVisibility(View.INVISIBLE);
        }

        if(isChecked&&!answer){
            imageView.setVisibility(View.VISIBLE);
                Picasso.get().load(R.mipmap.red_cross_icon).into(imageView);
        }
    }
}
